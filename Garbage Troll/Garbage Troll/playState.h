//(c) Michael O'Neil 2015

#ifndef PLAYSTATE_H   
#define PLAYSTATE_H   

#include "skyBox.h"
#include "immovableObjects.h"
#include "movableObjects.h"
#include "troll.h"
#include <sstream>

#define DEG_TO_RAD 0.017453293

/*
* Play State - Game is played here.
*/
class playState : public gameState {
private:			
	int score;
	int highestScore;
	int itemsHeld;
	GLuint textures[7];
	GLuint hudShader;	
	std::vector<GLfloat> verts;	
	clock_t timer;
	clock_t currentTime;
	clock_t endTime;
	int minutes;
	int seconds;
	bool lockControls;
	bool lostGame;
	bool wonGame;
	bool finalScore;
	std::stack<glm::mat4> mvStack; //stackfor modelview	
	glm::vec3 eye; //eye of camera
	glm::vec3 at; //at of camera
	glm::vec3 up; //up of camera
	GLfloat radius; 	
	glm::vec4 lightPos; 

	skyBox * sky_Box; 		
	immovableObjects * world_iObjects;	
	movableObjects * world_mObjects;
	troll * my_Player;	
	
	glm::vec3 moveForward(glm::vec3 cam, GLfloat angle, GLfloat d);	
	//collision functions
	void playerImmovableCollision();
	void playerMovableCollision();
	void movableImmovableCollision();
	void movableMovableCollision();
public:			
	playState(void);
	playState(GLuint mesh, GLuint count, GLuint shader, GLuint hShader, std::vector<GLfloat> v);
	~playState(void);
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void update(Game &context);	
	void enter(Game &context);
	void exit(Game &context);		
};

#endif 
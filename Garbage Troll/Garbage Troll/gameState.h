//(c) Michael O'Neil 2015

#include <stack>

class Game;

/*
* Abstract Game State Class.
* Different game states inherit from this.
*/
class gameState {
protected:
	std::stack<glm::mat4> mvStack; 	
	GLuint shaderProgram; 	
	GLuint meshObject;
	GLuint meshIndexCount;
public:	
	virtual ~gameState(void) { return; }
	virtual void draw(SDL_Window * window, Game &context) = 0;
	virtual void init(Game &context) = 0;
	virtual void update(Game &context) = 0;	
	virtual void enter(Game &context) = 0;
	virtual void exit(Game &context) = 0;	
};
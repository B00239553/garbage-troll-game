//(c) Michael O'Neil 2015

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <vector>

#ifndef AABB_H  
#define AABB_H

/*
* Bounding boxes for game.
*/
class AABB
{
private:
	std::string name;
	glm::vec3 min;
	glm::vec3 max;
	glm::vec3 centre;
	float length;
	float height;
	float depth;
public:
	AABB(void);
	AABB(std::string mName);
	~AABB(void);
	void setAABB(const std::vector<GLfloat> &verts, int vertCount);		
	void updateAABB(glm::vec3 pos);
	bool AABBcollision(AABB otherAABB);
	std::string getName(void) { return name; }
	glm::vec3 getMin(void) { return min; } 
	glm::vec3 getMax(void) { return max; } 
	glm::vec3 getCentre(void) { return centre; }
	float getLength(void) { return length; }
	float getHeight(void) { return height; }
	float getDepth(void) { return depth; }		
};

#endif
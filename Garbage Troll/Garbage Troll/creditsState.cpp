//(c) Michael O'Neil 2015

#include "game.h"
#include "creditsState.h"

/*
* Constructor.
*/
creditsState::creditsState(void)
{
}

/*
* Variable Constructor. Takes in variables so object is only loaded once.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for state.
*/
creditsState::creditsState(GLuint mesh, GLuint count, GLuint shader)
{	
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = shader;
}

/*
* Deconstructor.
*/
creditsState::~creditsState(void)
{
}

/*
* Create text textures.
* @param - Game - the context of the game.
*/
void creditsState::init(Game &context)
{			
	textures[0] = context.textToTexture("Credits", glm::vec3(255, 100, 0));	
	textures[1] = context.textToTexture("Created By", glm::vec3(255, 100, 0));
	textures[2] = context.textToTexture("Michael O'Neil", glm::vec3(255, 100, 0));	
}

/*
* Sets clock so can exit without interaction.
* @param - Game - the context of the game.
*/
void creditsState::enter(Game &context)
{		
	enterTime = clock();
	exitTime = clock() + 10000;
}

/*
* Displays the credits.
* @param - SDL_Window - window to draw to.
* @param - Game - game context.
*/
void creditsState::draw(SDL_Window * window, Game &context)
{		
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(glm::mat4(1.0f)));
	glDepthMask(GL_FALSE); // make sure depth test is off		
	glUseProgram(shaderProgram);

	glBindTexture(GL_TEXTURE_2D, textures[0]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.9f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.5f, 0.20f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();		
		
	glBindTexture(GL_TEXTURE_2D, textures[1]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.25f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.5f, 0.15f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();			
	
	glBindTexture(GL_TEXTURE_2D, textures[2]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.1f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.5f, 0.15f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();			
		
	glDepthMask(GL_TRUE);	
	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* @param - Game - the context of the game.
*/
void creditsState::update(Game &context)
{		
	Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_SPACE] || keys[SDL_SCANCODE_RETURN])
		context.setState(context.getMainMenuState());
	enterTime = clock();
	if(enterTime > exitTime)
		context.setState(context.getMainMenuState());

}

/*
* @param - Game - the context of the game.
*/
void creditsState::exit(Game &context)
{		
}
//(c) Michael O'Neil 2015

#include "skyBox.h"

/*
* Constructor.
*/
skyBox::skyBox(GLuint shader)
{
	skyBoxProgram = shader;
	//SkyBox's variables to draw a quad and have texture coordinates
	vertices[0] = -2;	vertices[1] = -2;	vertices[2] = 0; 
	vertices[3] = 2;	vertices[4] = 2;	vertices[5] = 0;  
	vertices[6] = 2;	vertices[7] = -2;	vertices[8] = 0;  
	vertices[9] = -2;	vertices[10] = -2;	vertices[11] = 0;  
	vertices[12] = -2;	vertices[13] = 2;	vertices[14] = 0; 
	vertices[15] = 2;	vertices[16] = 2;	vertices[17] = 0;  
	
	skyBoxIndices[0] = 0;	skyBoxIndices[1] = 1;	skyBoxIndices[2] = 2;
	skyBoxIndices[3] = 3;	skyBoxIndices[4] = 4;	skyBoxIndices[5] = 5;
	
	vertexTexCoord[0] = 1; 	vertexTexCoord[1] = 1;	vertexTexCoord[2] = 0;	vertexTexCoord[3] = 0;
	vertexTexCoord[4] = 0;	vertexTexCoord[5] = 1;	vertexTexCoord[6] = 1;	vertexTexCoord[7] = 1;	
	vertexTexCoord[8] = 1;	vertexTexCoord[9] = 0;	vertexTexCoord[10] = 0;	vertexTexCoord[11] = 0;	
	
	//Skybox radius
	radius = 0.1f;	
}

/*
* Deconstructor.
*/
skyBox::~skyBox(void)
{
}

/*
* Loads skybox textures and creates mesh.
*/
void skyBox::init(void)
{
	textures[0] = loadBitmap("front.bmp");
	textures[1] = loadBitmap("left.bmp");
	textures[2] = loadBitmap("right.bmp");
	textures[3] = loadBitmap("back.bmp");
	textures[4] = loadBitmap("top.bmp");
	meshObject = rt3d::createMesh(VERTEX_COUNT, vertices, nullptr, vertices, vertexTexCoord, INDEX_COUNT, skyBoxIndices);
}

/*
* Sets radius of skybox so is correct if player turns.
* @param - GLfloat - value to be set.
*/
void skyBox::setRadius(GLfloat rad)
{	
	radius = rad;	
}

/*
* Load bitmap file from name taken in.
* Will clamp to edge as used for the skybox.
* @param - char * - name of bitmap file.
* @return - GLuint - returns texture created from bitmap image.
*/
GLuint skyBox::loadBitmap(char *fname)
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	// load file - using core SDL library
 	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 

	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) 
	{
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else 
	{
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID;	// return value of texture ID
}

/*
* Skybox will be drawn here, rotating according to the game camera.
*/
void skyBox::draw(void)
{	
	// sets up projection matrix	
	glm::mat4 projection(1.0);		
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,50.0f);	
	
	//sets modelview
	glm::mat4 modelview(1.0);	
	mvStack.push(modelview); //push modelview to stack	
	mvStack.top() = glm::rotate(mvStack.top(), radius, glm::vec3(0.0f, 1.0f, 0.0f)); //allows scene to rotate according to radius
	
	glUseProgram(skyBoxProgram); //use skyBox shaders	
	rt3d::setUniformMatrix4fv(skyBoxProgram, "projection", glm::value_ptr(projection));
	glDepthMask(GL_FALSE); // make sure depth test is off

	for(int i = 0; i < TEX_COUNT; i++)
	{
		mvStack.push(mvStack.top());
		glBindTexture(GL_TEXTURE_2D, textures[i]);
		if(i == 0) //moves & rotates skybox front.
		{
			mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, -2.0f)); 
			mvStack.top() = glm::rotate(mvStack.top(), 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		} 
		if(i == 1) //left side
		{
			mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-2.0f, 0.0f, 0.0f));
			mvStack.top() = glm::rotate(mvStack.top(), 270.0f, glm::vec3(0.0f, 1.0f, 0.0f));	
		}
		if(i == 2) //right side
		{
			mvStack.top() = glm::translate(mvStack.top(), glm::vec3(2.0f, 0.0f, 0.0f));	
			mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));	
		}
		if(i == 3) //back		
			mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 2.0f));			
		if(i == 4) //top
		{
			mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 2.0f, 0.0f));	
			mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));	
			mvStack.top() = glm::rotate(mvStack.top(), 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));
		}
		rt3d::setUniformMatrix4fv(skyBoxProgram, "modelview", glm::value_ptr(mvStack.top()));
		rt3d::drawIndexedMesh(meshObject,INDEX_COUNT,GL_TRIANGLES);
		mvStack.pop();
	}	
}


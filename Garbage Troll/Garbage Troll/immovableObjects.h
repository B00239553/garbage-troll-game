//(c) Michael O'Neil 2015

#ifndef IMMOVEABLEOBJECTS_H  
#define IMMOVEABLEOBJECTS_H

#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "object.h"
#include <stack>
#include "AABB.h"

/*
* Class to create, draw immovable objects in the world.
*/
class immovableObjects
{
private:
	static const int MAX_OBJECTS = 27;
	object * worldObjects[MAX_OBJECTS];
	GLuint meshObject;
	GLuint meshIndexCount;
	GLuint texture;
	AABB * boxes[MAX_OBJECTS];
	bool AABBset;
	std::vector<GLfloat> verts;	
	GLuint loadBitmap(char *fname);
public:
	immovableObjects(void);
	immovableObjects(std::vector<GLfloat> v, GLuint mesh, GLuint count);
	~immovableObjects(void);
	void init(void);
	int getNumObjects(void) {return MAX_OBJECTS;}
	object *getObject(int obj) {return worldObjects[obj];}
	AABB *getAABB(int box) {return boxes[box];}
	void draw(std::stack<glm::mat4> mvStack, GLuint shader);	
};

#endif
//(c) Michael O'Neil 2015

#ifndef MAINMENUSTATE_H   
#define MAINMENUSTATE_H   

/*
* Main Menu State - will enter this state after intro
* or when game is paused. Will display possible options.
*/
class mainMenuState : public gameState {
private:		
	GLuint textures[24];	
	bool showInstructions;
	bool newGame;
	bool loseProgress;
	std::string playerName;
public:			
	mainMenuState(void);	
	mainMenuState(GLuint mesh, GLuint count, GLuint shader);
	~mainMenuState(void);
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void update(Game &context);	
	void enter(Game &context);
	void exit(Game &context);		
};

#endif 
//(c) Michael O'Neil 2015

#ifndef TABLESTATE_H   
#define TABLESTATE_H   

#include <sstream>

/*
* High Score Table - Will load the current table and save new table on exit.
*/
class tableState : public gameState {
private:	
	static const int MAX = 10;
	std::string names[MAX];
	int highScores[MAX];
	GLuint textures[12];	
	bool tableUpdated;
public:			
	tableState(void);	
	tableState(GLuint mesh, GLuint count, GLuint shader);
	~tableState(void);
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void update(Game &context);	
	void enter(Game &context);
	void exit(Game &context);		
};

#endif 
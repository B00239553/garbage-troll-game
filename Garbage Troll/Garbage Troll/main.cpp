//(c) Michael O'Neil 2015

#include "game.h"

// Opens console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
	Game *newGame = new Game();

	newGame->init();
	newGame->run();

	delete newGame;
    return 0;    
}
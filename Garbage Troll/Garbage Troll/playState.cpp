//(c) Michael O'Neil 2015

#include "game.h"
#include "playState.h"

/*
* Constructor.
*/
playState::playState(void)
{		
}

/*
* Variable Constructor. Takes in variables so object is only loaded once.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for game objects.
* @param - GLuint - shader program for HUD.
* @param - std::vector<GLfloat> - verts for objects.
*/
playState::playState(GLuint mesh, GLuint count, GLuint shader, GLuint hShader, std::vector<GLfloat> v)
{	
	score = 0;
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = shader;
	hudShader = hShader;
	verts = v;	
	//sets light position
	lightPos.x = -10.0f;	lightPos.y = 10.0f;				
	lightPos.z = 10.0f;	lightPos.w = 1.0f;		
}

/*
* Deconstructor.
*/
playState::~playState(void)
{
	delete sky_Box;
	sky_Box = NULL;
	delete world_iObjects;
	world_iObjects = NULL;		
}

/*
* Initialises classes that will not change during
* first to last game played.
* @param - Game - the context of the game.
*/
void playState::init(Game &context)
{	
	sky_Box = new skyBox(hudShader);
	sky_Box->init();	
	world_iObjects = new immovableObjects(verts, meshObject, meshIndexCount);
	world_iObjects->init();	
}

/*
* Sets certain variables depending on whether entering a new game
* or a continuing game from being paused.
* @param - Game - the context of the game.
*/
void playState::enter(Game &context)
{	
	lockControls = false;
	lostGame = false;
	wonGame = false;
	finalScore = false;
	currentTime = clock();
	if(context.getInGame() == false)
	{		
		score = 0; //reset score
		itemsHeld = 0;
		highestScore = 1100;
		std::stringstream strStream; //textures
		strStream << context.getScore();
		textures[0] = context.textToTexture("Score: ", glm::vec3(0, 0, 255));	
		textures[1] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));			
		up.x = 0.0f;	up.y = 1.0f;	up.z = 0.0f; //set/reset camera		
		radius = 0.0f;	
		sky_Box->setRadius(radius);
		timer = 180000; //set game time limit - 3 minutes
		my_Player = new troll(context.getName()); //create new player
		my_Player->init();	
		world_mObjects = new movableObjects(verts, meshObject, meshIndexCount); //create new movable objects
		world_mObjects->init();	
		textures[2] = context.textToTexture(my_Player->getName().c_str(), glm::vec3(0, 0, 255));
		context.setInGame(true);
	}	
	//timer functions and texture creation
	endTime = clock() + timer;
	minutes = timer / 60000;
	seconds = (timer - (minutes * 60000)) / 1000;
	std::stringstream strStream;
	if(seconds > -1 && seconds < 10)
		strStream << minutes << ":0" << seconds;
	else
		strStream << minutes << ":" << seconds;
	textures[3] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));
	strStream.str(std::string());
	strStream << "Items held: " << my_Player->getItemsHeld();
	textures[6] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));
}

/*
* Required to move the camera forwards and backwards.
* @param - glm::vec3 - the position of the troll.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 playState::moveForward(glm::vec3 cam, GLfloat angle, GLfloat d) {
	return glm::vec3(cam.x + d*std::sin(angle*DEG_TO_RAD), cam.y, cam.z - d*std::cos(angle*DEG_TO_RAD));
}

/*
* Main draw function for game play. Calls elements to be drawn, skyBox etc.
* Will then draw the HUD.
* Different draw sections will be called depending on game state: in play, won or lost.
* @param - SDL_Window - window to draw to.
* @param - Game - game context.
*/
void playState::draw(SDL_Window * window, Game &context)
{		
	// clear the screen
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);	
	glUseProgram(shaderProgram);
	//environment light
	rt3d::lightStruct lightEnvironment = {
		{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
		{0.7f, 0.7f, 0.7f, 1.0f}, // diffuse
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular
		{-10.0f, 10.0f, 10.0f, 1.0f}  // position
	};
	
	//sets light
	rt3d::setLight(shaderProgram, lightEnvironment);
	
	//draws the skyBox
	sky_Box->draw();	
	
	// set up projection matrix	
	glm::mat4 projection(1.0);		
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,50.0f);	
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection)); 

	glm::mat4 modelview(1.0);	
	mvStack.push(modelview); //push modelview to stack
	
	//sets camera
	at = my_Player->getPos();		
	eye = moveForward(at,radius,-3.0f);	
	eye.y += 0.5f; //this has been included so the eye is highly than the player y coordinate
	at.y += 0.5f;  //this has been included so the eye is highly than the player y coordinate
	mvStack.top() = glm::lookAt(eye,at,up);	//sets lookAt position

	//set light position, based on where camera is looking
	glm::vec4 tmp = mvStack.top()*lightPos;
	rt3d::setLightPos(shaderProgram, glm::value_ptr(tmp));

	glDepthMask(GL_TRUE); //turn on depth mask, must be turned on here as skybox drawn first	
	glUseProgram(shaderProgram);
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection)); 

	//draw troll
	my_Player->draw(mvStack, shaderProgram);
	//draw world immovable objects
	world_iObjects->draw(mvStack, shaderProgram);
	//draw world movable objects
	world_mObjects->draw(mvStack, shaderProgram);
		
	glDepthMask(GL_FALSE); // make sure depth test is off		
	glUseProgram(hudShader);
	rt3d::setUniformMatrix4fv(hudShader, "projection", glm::value_ptr(glm::mat4(1.0f)));

	if(lostGame == false && wonGame == false) //draws for game in play
	{
		glBindTexture(GL_TEXTURE_2D, textures[0]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.65f, 0.95f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.15f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[1]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.82f, 0.95f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.15f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();

		glBindTexture(GL_TEXTURE_2D, textures[2]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.8f, 0.95f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.3f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();

		glBindTexture(GL_TEXTURE_2D, textures[3]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.95f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.3f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();

		glBindTexture(GL_TEXTURE_2D, textures[6]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.8f, 0.85f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.3f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();
	}
	if(lostGame == true) //draws for game lost
	{
		glBindTexture(GL_TEXTURE_2D, textures[4]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.9f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.3f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();

		glBindTexture(GL_TEXTURE_2D, textures[5]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.65f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.3f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();
	}
	if(wonGame == true) //draws for game won
	{
		glBindTexture(GL_TEXTURE_2D, textures[4]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.95f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.3f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();

		glBindTexture(GL_TEXTURE_2D, textures[5]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.75f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.3f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();

		glBindTexture(GL_TEXTURE_2D, textures[3]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.55f, -1.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.3f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(hudShader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();
	}

	glDepthMask(GL_TRUE);
	mvStack.pop(); //last pop of stack		
	
	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* Will update certain game variables depending on what happens.
* @param - Game - the context of the game.
*/
void playState::update(Game &context)
{		
	//Call collision detection fucntions
	playerMovableCollision();
	playerImmovableCollision();	
	movableImmovableCollision();	
	movableMovableCollision();
		
	if(context.getScore() != score)//updates score and texture
	{
		context.setScore(score);
		std::stringstream strStream; 
		strStream << context.getScore();		
		textures[1] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));	
	}
	
	if(my_Player->getItemsHeld() != itemsHeld) //updates items held and texture
	{
		my_Player->setItemsHeld(itemsHeld);
		std::stringstream strStream;
		strStream << "Items held: " << my_Player->getItemsHeld();
		textures[6] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));
	}

	if(lockControls == false) //player controls if game in play
	{
		Uint8 *keys = SDL_GetKeyboardState(NULL);

		if ( keys[SDL_SCANCODE_P])
			context.setState(context.getMainMenuState());		
		if ( keys[SDL_SCANCODE_W] )
			my_Player->moveForward(radius);			
		else		
			my_Player->setAnimation(0);		
		if ( keys[SDL_SCANCODE_S] )	
			my_Player->moveBack(radius);				
		if ( keys[SDL_SCANCODE_A] )
			my_Player->moveLeft(radius);	
		if ( keys[SDL_SCANCODE_D] )	
			my_Player->moveRight(radius);	
		if ( keys[SDL_SCANCODE_SPACE] )
			my_Player->jump();
		if ( keys[SDL_SCANCODE_LEFT] ) //rotate camera right, rotates around player if player is stationary
		{	
			my_Player->setRad(radius);		
			radius -= 1.0f;
			sky_Box->setRadius(radius); //sets skybox radius to rotate correctly according to camera			
		}		
		if ( keys[SDL_SCANCODE_RIGHT] ) //rotate camera left, rotates around player if player is stationary
		{	
			my_Player->setRad(radius);		
			radius += 1.0f;
			sky_Box->setRadius(radius); //sets skybox radius so this also rotates correctly according to camera	
		}	
	}	

	if(lostGame == false && wonGame == false) //game still being played - update timer and its texture
	{
		currentTime = clock();
		timer = endTime - currentTime;
		minutes = timer / 60000;
		seconds = (timer - (minutes * 60000)) / 1000;
		std::stringstream strStream;
		if(seconds > -1 && seconds < 10)
			strStream << minutes << ":0" << seconds;
		else
			strStream << minutes << ":" << seconds;
		if(timer < 30000)
			textures[3] = context.textToTexture(strStream.str().c_str(), glm::vec3(255, 0, 0));
		else
			textures[3] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));
	}
	
	if(timer <= 0 && lostGame == false && wonGame == false) //game lost, tell player and display final score.
	{									
		timer = 10000; //set new timer for losing sequence
		endTime = clock() + timer;
		minutes = 0;
		seconds = 0;
		std::stringstream strStream; //losing textures
		strStream << minutes << ":0" << seconds;
		textures[3] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));
		lockControls = true;
		lostGame = true;
		textures[4] = context.textToTexture("You Lose", glm::vec3(255, 0, 0));
		strStream.str(std::string());
		strStream << "Final Score: " << context.getScore();
		textures[5] = context.textToTexture(strStream.str().c_str(), glm::vec3(255, 0, 0));
	}

	if(lostGame == true) //locks controls and spins camera with troll animation
	{					 //shows for 10 seconds then exits to high score table
		currentTime = clock();
		timer = endTime - currentTime;
		my_Player->setRad(radius);		
		radius -= 1.0f;
		sky_Box->setRadius(radius);
		my_Player->setAnimation(7);
		if(timer <= 0)
		{			
			context.setInGame(false);
			context.setState(context.getTableState());
		}
	}

	if(score == highestScore && wonGame == false) //game won
	{			
		std::stringstream strStream; //win textures		
		if(seconds > -1 && seconds < 10)
			strStream << "Remaining Time: " << minutes << ":0" << seconds;
		else
			strStream << "Remaining Time: " << minutes << ":" << seconds;
		textures[3] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));
		lockControls = true;
		wonGame = true;
		textures[4] = context.textToTexture("You Win", glm::vec3(0, 0, 255));
		strStream.str(std::string());
		strStream << "Final Score: " << context.getScore();
		textures[5] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));		
		endTime = clock() + 5000;		
	}

	if(wonGame == true)  //sequence after game has been won
	{					 //will display final score for 5 seconds, then will add remaining time onto score
		currentTime = clock();		
		if(currentTime >= endTime)
		{
			if(timer > 0)
			{
				timer -= 1000;
				minutes = timer / 60000;
				seconds = (timer - (minutes * 60000)) / 1000;
				score += 10; 				
			}	
			else if(finalScore == false && timer <= 0)
			{
				endTime = clock() + 3000;
				finalScore = true;
			}
				
			std::stringstream strStream; //win textures		
			if(seconds > -1 && seconds < 10)
				strStream << "Remaining Time: " << minutes << ":0" << seconds;
			else
				strStream << "Remaining Time: " << minutes << ":" << seconds;
			textures[3] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));
			strStream.str(std::string());
			strStream << "Final Score: " << context.getScore();
			textures[5] = context.textToTexture(strStream.str().c_str(), glm::vec3(0, 0, 255));	
		}
		my_Player->setRad(radius);		
		radius -= 1.0f;
		sky_Box->setRadius(radius);
		my_Player->setAnimation(9);	

		if(finalScore == true && currentTime >= endTime) //time has been added onto score, exit play state
		{
			context.setInGame(false);
			context.setState(context.getTableState());
		}		
	}		
}

/*
* Deletes player and movable objects if game ends, 
* this is because they are recreated if there is a new game.
* @param - Game - the context of the game.
*/
void playState::exit(Game &context)
{		
	if(context.getInGame() == false)
	{
		delete my_Player;
		my_Player = NULL;
		delete world_mObjects;
		world_mObjects = NULL;
	}
}

/*
* Player vs ImmovableObjects.
*/
void playState::playerImmovableCollision()
{
	for(int count = 0; count < world_iObjects->getNumObjects(); count++)
	{
		if(world_iObjects->getObject(count)->getType() == "Road")
				if(my_Player->getAABB()->getMin().x >= world_iObjects->getAABB(count)->getMin().x
					&& my_Player->getAABB()->getMax().x  <= world_iObjects->getAABB(count)->getMax().x
					&& my_Player->getAABB()->getMin().z >= world_iObjects->getAABB(count)->getMin().z
					&& my_Player->getAABB()->getMax().z <= world_iObjects->getAABB(count)->getMax().z)			
						my_Player->setPos(glm::vec3(my_Player->getPos().x, 0.5f, my_Player->getPos().z));		
		if(world_iObjects->getObject(count)->getType() == "Ground")
				if(my_Player->getAABB()->getMin().x >= world_iObjects->getAABB(count)->getMin().x
					&& my_Player->getAABB()->getMax().x  <= world_iObjects->getAABB(count)->getMax().x
					&& my_Player->getAABB()->getMin().z >= world_iObjects->getAABB(count)->getMin().z
					&& my_Player->getAABB()->getMax().z <= world_iObjects->getAABB(count)->getMax().z)			
						my_Player->setPos(glm::vec3(my_Player->getPos().x, 0.6f, my_Player->getPos().z));

		if(my_Player->getAABB()->AABBcollision(*world_iObjects->getAABB(count)) == true)
		{					
			if(world_iObjects->getObject(count)->getType() == "Road" || world_iObjects->getObject(count)->getType() == "Ground")
				; //ignore
			else
			{		
				if(my_Player->getAABB()->getMin().x >= world_iObjects->getAABB(count)->getMin().x && my_Player->getAABB()->getMax().x <= world_iObjects->getAABB(count)->getMax().x
					&& my_Player->getAABB()->getMin().z <= world_iObjects->getAABB(count)->getMax().z && my_Player->getAABB()->getMax().z >= world_iObjects->getAABB(count)->getMax().z)
				{					
					my_Player->setPos(glm::vec3(my_Player->getPos().x, my_Player->getPos().y, 
						(world_iObjects->getAABB(count)->getMax().z + my_Player->getAABB()->getDepth()/2)));											
				}
				if(my_Player->getAABB()->getMin().x >= world_iObjects->getAABB(count)->getMin().x && my_Player->getAABB()->getMax().x <= world_iObjects->getAABB(count)->getMax().x
					&& my_Player->getAABB()->getMin().z <= world_iObjects->getAABB(count)->getMin().z && my_Player->getAABB()->getMax().z >= world_iObjects->getAABB(count)->getMin().z)
				{					
					my_Player->setPos(glm::vec3(my_Player->getPos().x, my_Player->getPos().y, 
						(world_iObjects->getAABB(count)->getMin().z - my_Player->getAABB()->getDepth()/2)));											
				}
				if(my_Player->getAABB()->getMin().z >= world_iObjects->getAABB(count)->getMin().z && my_Player->getAABB()->getMax().z <= world_iObjects->getAABB(count)->getMax().z
					&& my_Player->getAABB()->getMin().x <= world_iObjects->getAABB(count)->getMin().x && my_Player->getAABB()->getMax().x >= world_iObjects->getAABB(count)->getMin().x)
				{				
					my_Player->setPos(glm::vec3((world_iObjects->getAABB(count)->getMin().x - my_Player->getAABB()->getLength()/2), my_Player->getPos().y, 
						my_Player->getPos().z));											
				}
				if(my_Player->getAABB()->getMin().z >= world_iObjects->getAABB(count)->getMin().z && my_Player->getAABB()->getMax().z <= world_iObjects->getAABB(count)->getMax().z
					&& my_Player->getAABB()->getMax().x >= world_iObjects->getAABB(count)->getMax().x && my_Player->getAABB()->getMin().x <= world_iObjects->getAABB(count)->getMax().x)
				{				
					my_Player->setPos(glm::vec3((world_iObjects->getAABB(count)->getMax().x + my_Player->getAABB()->getLength()/2), my_Player->getPos().y, 
						my_Player->getPos().z));											
				}
				if(my_Player->getAABB()->getMin().x <= world_iObjects->getAABB(count)->getMax().x && my_Player->getAABB()->getMax().x >= world_iObjects->getAABB(count)->getMax().x
					&& my_Player->getAABB()->getMin().z <= world_iObjects->getAABB(count)->getMax().z && my_Player->getAABB()->getMax().z >= world_iObjects->getAABB(count)->getMax().z)
				{				
					my_Player->setPos(glm::vec3((world_iObjects->getAABB(count)->getMax().x + my_Player->getAABB()->getLength()/2), my_Player->getPos().y, 
						(world_iObjects->getAABB(count)->getMax().z + my_Player->getAABB()->getDepth()/2)));											
				}
				if(my_Player->getAABB()->getMin().x <= world_iObjects->getAABB(count)->getMin().x && my_Player->getAABB()->getMax().x >= world_iObjects->getAABB(count)->getMin().x
					&& my_Player->getAABB()->getMin().z <= world_iObjects->getAABB(count)->getMax().z && my_Player->getAABB()->getMax().z >= world_iObjects->getAABB(count)->getMax().z)
				{				
					my_Player->setPos(glm::vec3((world_iObjects->getAABB(count)->getMin().x - my_Player->getAABB()->getLength()/2), my_Player->getPos().y, 
						(world_iObjects->getAABB(count)->getMax().z + my_Player->getAABB()->getDepth()/2)));											
				}
				if(my_Player->getAABB()->getMin().x <= world_iObjects->getAABB(count)->getMin().x && my_Player->getAABB()->getMax().x >= world_iObjects->getAABB(count)->getMin().x
					&& my_Player->getAABB()->getMin().z <= world_iObjects->getAABB(count)->getMin().z && my_Player->getAABB()->getMax().z >= world_iObjects->getAABB(count)->getMin().z)
				{				
					my_Player->setPos(glm::vec3((world_iObjects->getAABB(count)->getMin().x - my_Player->getAABB()->getLength()/2), my_Player->getPos().y, 
						(world_iObjects->getAABB(count)->getMin().z + my_Player->getAABB()->getDepth()/2)));											
				}
				if(my_Player->getAABB()->getMin().x <= world_iObjects->getAABB(count)->getMax().x && my_Player->getAABB()->getMax().x >= world_iObjects->getAABB(count)->getMax().x
					&& my_Player->getAABB()->getMin().z <= world_iObjects->getAABB(count)->getMin().z && my_Player->getAABB()->getMax().z >= world_iObjects->getAABB(count)->getMin().z)
				{				
					my_Player->setPos(glm::vec3((world_iObjects->getAABB(count)->getMax().x + my_Player->getAABB()->getLength()/2), my_Player->getPos().y, 
						(world_iObjects->getAABB(count)->getMin().z + my_Player->getAABB()->getDepth()/2)));											
				}
			}
		}
		else //no collision with buildings, update player AABB as normal
			my_Player->getAABB()->updateAABB(my_Player->getPos());				
	}
}

/*
* Player vs movableObjects.
*/
void playState::playerMovableCollision()
{			
	for(int count = 0; count < world_mObjects->getNumObjects(); count++)
	{				
		if(my_Player->getAABB()->AABBcollision(*world_mObjects->getAABB(count)) == true)
		{				
			if(world_mObjects->getObject(count)->getType() == "Garbage")
			{
				if(my_Player->getItemsHeld() < 2)
				{
					itemsHeld++;
					score += 10;
					world_mObjects->deleteObject(world_mObjects->getObject(count));
				}				
			}
			else if(world_mObjects->getAABB(count)->getMin().x >= my_Player->getAABB()->getMin().x && world_mObjects->getAABB(count)->getMax().x <= my_Player->getAABB()->getMax().x
			&& world_mObjects->getAABB(count)->getMax().z >= my_Player->getAABB()->getMin().z && world_mObjects->getAABB(count)->getMax().z <= my_Player->getAABB()->getMax().z)
			{					
				world_mObjects->getObject(count)->setTranslate(glm::vec3(world_mObjects->getObject(count)->getTranslate().x, world_mObjects->getObject(count)->getTranslate().y, 
					(world_mObjects->getObject(count)->getTranslate().z - 0.1f)));	
				if(my_Player->getItemsHeld() > 0)
				{
					score += 100 * my_Player->getItemsHeld();
					itemsHeld = 0;				
				}
			}
			else if(world_mObjects->getAABB(count)->getMin().x >= my_Player->getAABB()->getMin().x && world_mObjects->getAABB(count)->getMax().x <= my_Player->getAABB()->getMax().x
			&& world_mObjects->getAABB(count)->getMin().z <= my_Player->getAABB()->getMax().z && world_mObjects->getAABB(count)->getMin().z >= my_Player->getAABB()->getMin().z)
			{					
				world_mObjects->getObject(count)->setTranslate(glm::vec3(world_mObjects->getObject(count)->getTranslate().x, world_mObjects->getObject(count)->getTranslate().y, 
					(world_mObjects->getObject(count)->getTranslate().z + 0.1f)));	
				if(my_Player->getItemsHeld() > 0)
				{
					score += 100 * my_Player->getItemsHeld();
					itemsHeld = 0;				
				}
			}
			else if(world_mObjects->getAABB(count)->getMin().z >= my_Player->getAABB()->getMin().z && world_mObjects->getAABB(count)->getMax().z <= my_Player->getAABB()->getMax().z
			&& world_mObjects->getAABB(count)->getMax().x >= my_Player->getAABB()->getMin().x && world_mObjects->getAABB(count)->getMax().x <= my_Player->getAABB()->getMax().x)
			{					
				world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x - 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
					world_mObjects->getObject(count)->getTranslate().z));		
				if(my_Player->getItemsHeld() > 0)
				{
					score += 100 * my_Player->getItemsHeld();
					itemsHeld = 0;					
				}
			}
			else if(world_mObjects->getAABB(count)->getMin().z >= my_Player->getAABB()->getMin().z && world_mObjects->getAABB(count)->getMax().z <= my_Player->getAABB()->getMax().z
			&& world_mObjects->getAABB(count)->getMin().x <= my_Player->getAABB()->getMax().x && world_mObjects->getAABB(count)->getMin().x >= my_Player->getAABB()->getMin().x)
			{					
				world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x + 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
					world_mObjects->getObject(count)->getTranslate().z));	
				if(my_Player->getItemsHeld() > 0)
				{
					score += 100 * my_Player->getItemsHeld();
					itemsHeld = 0;				
				}
			}
			else if(world_mObjects->getAABB(count)->getMax().x >= my_Player->getAABB()->getMin().x && world_mObjects->getAABB(count)->getMax().x <= my_Player->getAABB()->getMax().x
			&& world_mObjects->getAABB(count)->getMax().z >= my_Player->getAABB()->getMin().z && world_mObjects->getAABB(count)->getMax().z <= my_Player->getAABB()->getMax().z)
			{				
				world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x - 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
					(world_mObjects->getObject(count)->getTranslate().z - 0.1f)));	
				if(my_Player->getItemsHeld() > 0)
				{
					score += 100 * my_Player->getItemsHeld();
					itemsHeld = 0;				
				}
			}
			else if(world_mObjects->getAABB(count)->getMin().x >= my_Player->getAABB()->getMin().x && world_mObjects->getAABB(count)->getMin().x <= my_Player->getAABB()->getMax().x
			&& world_mObjects->getAABB(count)->getMax().z >= my_Player->getAABB()->getMin().z && world_mObjects->getAABB(count)->getMax().z <= my_Player->getAABB()->getMax().z)
			{				
				world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x + 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
					(world_mObjects->getObject(count)->getTranslate().z - 0.1f)));		
				if(my_Player->getItemsHeld() > 0)
				{
					score += 100 * my_Player->getItemsHeld();
					itemsHeld = 0;					
				}
			}
			else if(world_mObjects->getAABB(count)->getMax().x >= my_Player->getAABB()->getMin().x && world_mObjects->getAABB(count)->getMax().x <= my_Player->getAABB()->getMax().x
			&& world_mObjects->getAABB(count)->getMin().z >= my_Player->getAABB()->getMin().z && world_mObjects->getAABB(count)->getMin().z <= my_Player->getAABB()->getMax().z)
			{				
				world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x - 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
					(world_mObjects->getObject(count)->getTranslate().z + 0.1f)));	
				if(my_Player->getItemsHeld() > 0)
				{
					score += 100 * my_Player->getItemsHeld();
					itemsHeld = 0;					
				}
			}
			else if(world_mObjects->getAABB(count)->getMin().x >= my_Player->getAABB()->getMin().x &&world_mObjects->getAABB(count)->getMin().x <= my_Player->getAABB()->getMax().x
			&& world_mObjects->getAABB(count)->getMin().z >= my_Player->getAABB()->getMin().z && world_mObjects->getAABB(count)->getMin().z <= my_Player->getAABB()->getMax().z)
			{				
				world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x + 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
					(world_mObjects->getObject(count)->getTranslate().z + 0.1f)));		
				if(my_Player->getItemsHeld() > 0)
				{
					score += 100 * my_Player->getItemsHeld();
					itemsHeld = 0;					
				}
			}			
		}				
	}	
}

/*
* movableObjects vs immovableObjects.
*/
void playState::movableImmovableCollision()
{
	for(int i = 0; i < world_mObjects->getNumObjects(); i++)
	{
		for(int count = 0; count < world_iObjects->getNumObjects(); count++)
		{
			if(world_mObjects->getObject(i)->getType() == "Bin" && world_iObjects->getObject(count)->getType() == "Road")
				if(world_mObjects->getAABB(i)->getMin().x >= world_iObjects->getAABB(count)->getMin().x
				&& world_mObjects->getAABB(i)->getMax().x  <= world_iObjects->getAABB(count)->getMax().x
				&& world_mObjects->getAABB(i)->getMin().z >= world_iObjects->getAABB(count)->getMin().z
				&& world_mObjects->getAABB(i)->getMax().z <= world_iObjects->getAABB(count)->getMax().z)			
					world_mObjects->getObject(i)->setTranslate(glm::vec3(world_mObjects->getObject(i)->getTranslate().x
						, 0.35f, world_mObjects->getObject(i)->getTranslate().z));		
			if(world_mObjects->getObject(i)->getType() == "Bin" && world_iObjects->getObject(count)->getType() == "Ground")
				if(world_mObjects->getAABB(i)->getMin().x >= world_iObjects->getAABB(count)->getMin().x
				&& world_mObjects->getAABB(i)->getMax().x  <= world_iObjects->getAABB(count)->getMax().x
				&& world_mObjects->getAABB(i)->getMin().z >= world_iObjects->getAABB(count)->getMin().z
				&& world_mObjects->getAABB(i)->getMax().z <= world_iObjects->getAABB(count)->getMax().z)			
					world_mObjects->getObject(i)->setTranslate(glm::vec3(world_mObjects->getObject(i)->getTranslate().x
						, 0.45f, world_mObjects->getObject(i)->getTranslate().z));

			if(world_mObjects->getAABB(i)->AABBcollision(*world_iObjects->getAABB(count)) == true)
			{					
				if(world_mObjects->getObject(i)->getType() == "Garbage" && world_iObjects->getObject(count)->getType() == "Road"
				|| world_iObjects->getObject(count)->getType() == "Ground")
					; //ignore
				else
				{		
					if(world_mObjects->getAABB(i)->getMin().x >= world_iObjects->getAABB(count)->getMin().x && world_mObjects->getAABB(i)->getMax().x <= world_iObjects->getAABB(count)->getMax().x
					&& world_mObjects->getAABB(i)->getMin().z <= world_iObjects->getAABB(count)->getMax().z && world_mObjects->getAABB(i)->getMax().z >= world_iObjects->getAABB(count)->getMax().z)
					{		
						world_mObjects->getObject(i)->setTranslate(glm::vec3(world_mObjects->getObject(i)->getTranslate().x, world_mObjects->getObject(i)->getTranslate().y,
							(world_iObjects->getAABB(count)->getMax().z + world_mObjects->getAABB(i)->getDepth()/2)));																
					}
					if(world_mObjects->getAABB(i)->getMin().x >= world_iObjects->getAABB(count)->getMin().x && world_mObjects->getAABB(i)->getMax().x <= world_iObjects->getAABB(count)->getMax().x
					&& world_mObjects->getAABB(i)->getMin().z <= world_iObjects->getAABB(count)->getMin().z && world_mObjects->getAABB(i)->getMax().z >= world_iObjects->getAABB(count)->getMin().z)
					{		
						world_mObjects->getObject(i)->setTranslate(glm::vec3(world_mObjects->getObject(i)->getTranslate().x, world_mObjects->getObject(i)->getTranslate().y,
							(world_iObjects->getAABB(count)->getMin().z - world_mObjects->getAABB(i)->getDepth()/2)));																
					}
					if(world_mObjects->getAABB(i)->getMin().z >= world_iObjects->getAABB(count)->getMin().z && world_mObjects->getAABB(i)->getMax().z <= world_iObjects->getAABB(count)->getMax().z
					&& world_mObjects->getAABB(i)->getMin().x <= world_iObjects->getAABB(count)->getMin().x && world_mObjects->getAABB(i)->getMax().x >= world_iObjects->getAABB(count)->getMin().x)
					{	
						world_mObjects->getObject(i)->setTranslate(glm::vec3((world_iObjects->getAABB(count)->getMin().x - world_mObjects->getAABB(i)->getLength()/2),
							world_mObjects->getObject(i)->getTranslate().y, world_mObjects->getObject(i)->getTranslate().z));															
					}
					if(world_mObjects->getAABB(i)->getMin().z >= world_iObjects->getAABB(count)->getMin().z && world_mObjects->getAABB(i)->getMax().z <= world_iObjects->getAABB(count)->getMax().z
					&& world_mObjects->getAABB(i)->getMax().x >= world_iObjects->getAABB(count)->getMax().x && world_mObjects->getAABB(i)->getMin().x <= world_iObjects->getAABB(count)->getMax().x)
					{				
						world_mObjects->getObject(i)->setTranslate(glm::vec3((world_iObjects->getAABB(count)->getMax().x + world_mObjects->getAABB(i)->getLength()/2),
							world_mObjects->getObject(i)->getTranslate().y, world_mObjects->getObject(i)->getTranslate().z));										
					}
					if(world_mObjects->getAABB(i)->getMin().x <= world_iObjects->getAABB(count)->getMax().x && world_mObjects->getAABB(i)->getMax().x >= world_iObjects->getAABB(count)->getMax().x
					&& world_mObjects->getAABB(i)->getMin().z <= world_iObjects->getAABB(count)->getMax().z && world_mObjects->getAABB(i)->getMax().z >= world_iObjects->getAABB(count)->getMax().z)
					{	
						world_mObjects->getObject(i)->setTranslate(glm::vec3((world_iObjects->getAABB(count)->getMax().x + world_mObjects->getAABB(i)->getLength()/2),
							world_mObjects->getObject(i)->getTranslate().y, (world_iObjects->getAABB(count)->getMax().z + world_mObjects->getAABB(i)->getDepth()/2)));																
					}
					if(world_mObjects->getAABB(i)->getMin().x <= world_iObjects->getAABB(count)->getMin().x && world_mObjects->getAABB(i)->getMax().x >= world_iObjects->getAABB(count)->getMin().x
					&& world_mObjects->getAABB(i)->getMin().z <= world_iObjects->getAABB(count)->getMax().z && world_mObjects->getAABB(i)->getMax().z >= world_iObjects->getAABB(count)->getMax().z)
					{	
						world_mObjects->getObject(i)->setTranslate(glm::vec3((world_iObjects->getAABB(count)->getMin().x - world_mObjects->getAABB(i)->getLength()/2),
							world_mObjects->getObject(i)->getTranslate().y, (world_iObjects->getAABB(count)->getMax().z + world_mObjects->getAABB(i)->getDepth()/2)));											
					}
					if(world_mObjects->getAABB(i)->getMin().x <= world_iObjects->getAABB(count)->getMin().x && world_mObjects->getAABB(i)->getMax().x >= world_iObjects->getAABB(count)->getMin().x
					&& world_mObjects->getAABB(i)->getMin().z <= world_iObjects->getAABB(count)->getMin().z && world_mObjects->getAABB(i)->getMax().z >= world_iObjects->getAABB(count)->getMin().z)
					{	
						world_mObjects->getObject(i)->setTranslate(glm::vec3((world_iObjects->getAABB(count)->getMin().x - world_mObjects->getAABB(i)->getLength()/2),
							world_mObjects->getObject(i)->getTranslate().y, (world_iObjects->getAABB(count)->getMin().z + world_mObjects->getAABB(i)->getDepth()/2)));																
					}
					if(world_mObjects->getAABB(i)->getMin().x <= world_iObjects->getAABB(count)->getMax().x && world_mObjects->getAABB(i)->getMax().x >= world_iObjects->getAABB(count)->getMax().x
					&& world_mObjects->getAABB(i)->getMin().z <= world_iObjects->getAABB(count)->getMin().z && world_mObjects->getAABB(i)->getMax().z >= world_iObjects->getAABB(count)->getMin().z)
					{		
						world_mObjects->getObject(i)->setTranslate(glm::vec3((world_iObjects->getAABB(count)->getMax().x + world_mObjects->getAABB(i)->getLength()/2),
							world_mObjects->getObject(i)->getTranslate().y, (world_iObjects->getAABB(count)->getMin().z + world_mObjects->getAABB(i)->getDepth()/2)));															
					}
				}
			}		
		}				
	}
}

/*
* movableObjects vs movableObjects.
*/
void playState::movableMovableCollision()
{
	for(int i = 0; i < world_mObjects->getNumObjects() - 1; i++)
	{
		for(int count = i+1; count < world_mObjects->getNumObjects(); count++)
		{				
			if(world_mObjects->getAABB(i)->AABBcollision(*world_mObjects->getAABB(count)) == true)
			{					
				if(world_mObjects->getAABB(count)->getMin().x >= world_mObjects->getAABB(i)->getMin().x && world_mObjects->getAABB(count)->getMax().x <= world_mObjects->getAABB(i)->getMax().x
				&& world_mObjects->getAABB(count)->getMax().z >= world_mObjects->getAABB(i)->getMin().z && world_mObjects->getAABB(count)->getMax().z <= world_mObjects->getAABB(i)->getMax().z)
				{					
					world_mObjects->getObject(count)->setTranslate(glm::vec3(world_mObjects->getObject(count)->getTranslate().x, world_mObjects->getObject(count)->getTranslate().y, 
						(world_mObjects->getObject(count)->getTranslate().z - 0.1f)));						
				}
				else if(world_mObjects->getAABB(count)->getMin().x >= world_mObjects->getAABB(i)->getMin().x && world_mObjects->getAABB(count)->getMax().x <= world_mObjects->getAABB(i)->getMax().x
				&& world_mObjects->getAABB(count)->getMin().z <= world_mObjects->getAABB(i)->getMax().z && world_mObjects->getAABB(count)->getMin().z >= world_mObjects->getAABB(i)->getMin().z)
				{					
					world_mObjects->getObject(count)->setTranslate(glm::vec3(world_mObjects->getObject(count)->getTranslate().x, world_mObjects->getObject(count)->getTranslate().y, 
						(world_mObjects->getObject(count)->getTranslate().z + 0.1f)));					
				}
				else if(world_mObjects->getAABB(count)->getMin().z >= world_mObjects->getAABB(i)->getMin().z && world_mObjects->getAABB(count)->getMax().z <= world_mObjects->getAABB(i)->getMax().z
				&& world_mObjects->getAABB(count)->getMax().x >= world_mObjects->getAABB(i)->getMin().x && world_mObjects->getAABB(count)->getMax().x <= world_mObjects->getAABB(i)->getMax().x)
				{					
					world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x - 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
						world_mObjects->getObject(count)->getTranslate().z));					
				}
				else if(world_mObjects->getAABB(count)->getMin().z >= world_mObjects->getAABB(i)->getMin().z && world_mObjects->getAABB(count)->getMax().z <= world_mObjects->getAABB(i)->getMax().z
					&& world_mObjects->getAABB(count)->getMin().x <= world_mObjects->getAABB(i)->getMax().x && world_mObjects->getAABB(count)->getMin().x >= world_mObjects->getAABB(i)->getMin().x)
				{					
					world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x + 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
						world_mObjects->getObject(count)->getTranslate().z));						
				}
				else if(world_mObjects->getAABB(count)->getMax().x >= world_mObjects->getAABB(i)->getMin().x && world_mObjects->getAABB(count)->getMax().x <= world_mObjects->getAABB(i)->getMax().x
				&& world_mObjects->getAABB(count)->getMax().z >= world_mObjects->getAABB(i)->getMin().z && world_mObjects->getAABB(count)->getMax().z <= world_mObjects->getAABB(i)->getMax().z)
				{				
					world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x - 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
						(world_mObjects->getObject(count)->getTranslate().z - 0.1f)));										
				}
				else if(world_mObjects->getAABB(count)->getMin().x >= world_mObjects->getAABB(i)->getMin().x && world_mObjects->getAABB(count)->getMin().x <= world_mObjects->getAABB(i)->getMax().x
				&& world_mObjects->getAABB(count)->getMax().z >= world_mObjects->getAABB(i)->getMin().z && world_mObjects->getAABB(count)->getMax().z <= world_mObjects->getAABB(i)->getMax().z)
				{				
					world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x + 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
						(world_mObjects->getObject(count)->getTranslate().z - 0.1f)));										
				}
				else if(world_mObjects->getAABB(count)->getMax().x >= world_mObjects->getAABB(i)->getMin().x && world_mObjects->getAABB(count)->getMax().x <= world_mObjects->getAABB(i)->getMax().x
				&& world_mObjects->getAABB(count)->getMin().z >= world_mObjects->getAABB(i)->getMin().z && world_mObjects->getAABB(count)->getMin().z <= world_mObjects->getAABB(i)->getMax().z)
				{				
					world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x - 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
						(world_mObjects->getObject(count)->getTranslate().z + 0.1f)));										
				}
				else if(world_mObjects->getAABB(count)->getMin().x >= world_mObjects->getAABB(i)->getMin().x &&world_mObjects->getAABB(count)->getMin().x <= world_mObjects->getAABB(i)->getMax().x
				&& world_mObjects->getAABB(count)->getMin().z >=world_mObjects->getAABB(i)->getMin().z && world_mObjects->getAABB(count)->getMin().z <= world_mObjects->getAABB(i)->getMax().z)
				{				
					world_mObjects->getObject(count)->setTranslate(glm::vec3((world_mObjects->getObject(count)->getTranslate().x + 0.1f), world_mObjects->getObject(count)->getTranslate().y, 
						(world_mObjects->getObject(count)->getTranslate().z + 0.1f)));										
				}				
			}				
		}	
	}	
}
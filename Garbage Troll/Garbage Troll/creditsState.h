//(c) Michael O'Neil 2015

#ifndef CREDITSSTATE_H   
#define CREDITSSTATE_H   

/*
* Credits State - displays credits, goes back to
* main menu after 10 seconds.
*/
class creditsState : public gameState {
private:		
	GLuint textures[3];	
	clock_t enterTime;
	clock_t exitTime;
public:			
	creditsState(void);
	creditsState(GLuint mesh, GLuint count, GLuint shader);
	~creditsState(void);
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void update(Game &context);	
	void enter(Game &context);
	void exit(Game &context);		
};

#endif 
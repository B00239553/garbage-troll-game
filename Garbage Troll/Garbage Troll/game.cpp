//(c) Michael O'Neil 2015

#include "game.h"
#include "introState.h"
#include "mainMenuState.h"
#include "creditsState.h"
#include "playState.h"
#include "tableState.h"

/*
* Constructor.
*/
Game::Game(void)
{	
	running = true;
	inGame = false;	
	playerScore = 0;
	playerName = "Player";
}

/*
* Deconstructor.
*/
Game::~Game(void)
{
	delete currentState;
	currentState = NULL;
	delete State_Intro;
	State_Intro = NULL;	
	delete State_Credits;
	State_Credits = NULL;
	delete State_Play;	
	State_Play = NULL;	
	delete State_Table;
	State_Table = NULL;
}

/*
* Loads the SDLcontext, glContext, shaders and cube object so only required 
* to be loaded once as used in different sections. Initialises states.
*/
void Game::init(void)
{	
	hWindow = setupRC(glContext); // Create window and render context 

	if (TTF_Init()== -1)
		std::cout << "TTF failed to initialise." << endl;
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		std::cout << "Failed to open font." << endl;
	
	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl; 
		exit (1);
	}
	std::cout << glGetString(GL_VERSION) << endl;	
		
	//loads shaders here so only required once
	shaderProgram = rt3d::initShaders("phong-tex.vert", "phong-tex.frag");
	depthOffProgram = rt3d::initShaders("skyBox.vert", "skyBox.frag");
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
			
	//Load cube object here, and create mesh so only required once.
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;		
	rt3d::loadObj("cube.txt", verts, norms, tex_coords, indices);	
	GLuint meshIndexCount = indices.size();
	meshObject = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), meshIndexCount, indices.data());
	
	//initialise states
	State_Intro = new introState(meshObject, meshIndexCount, depthOffProgram);
	State_Intro->init(*this);
	State_MainMenu = new mainMenuState(meshObject, meshIndexCount, depthOffProgram);
	State_MainMenu->init(*this);
	State_Credits = new creditsState(meshObject, meshIndexCount, depthOffProgram);
	State_Credits->init(*this);
	State_Play = new playState(meshObject, meshIndexCount, shaderProgram, depthOffProgram, verts);
	State_Play->init(*this);
	State_Table = new tableState(meshObject, meshIndexCount, depthOffProgram);
	State_Table->init(*this);
	currentState = State_Intro;	
}

/*
* Set up rendering context.
* @param - SDL_GLContext - the context to be set up.
* @return - SDL_Window - window to be drawn to.
*/
SDL_Window * Game::setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("Garbage Troll", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

/*
* Creates a texture with the text taken in.
* @param - const char * - text for texture.
* @param - glm::vec3 - colour of texture.
* @param - return - GLuint - the texture created.
*/
GLuint Game::textToTexture(const char * str, glm::vec3 col)
{	
	TTF_Font *font = textFont;
	SDL_Color colour = { col.x, col.y, col.z };
	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(textFont,str,colour);

	if (stringImage == NULL)		
		std::cout << "String surface not created." << std::endl;

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;
	
	GLuint format, internalFormat;
	if (colours == 4)
	{   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	}
	else 
	{   // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	return texture;
}

/*
* Used for changing states, it will perform any exit requirements
* for the current state. It will then enter the new state and perform any
* entry requirements.
* @param - gameState - The new game state to be entered.
*/
void Game::setState(gameState* newState)
{
	if(newState != currentState)  
	{		
		currentState->exit(*this);
		currentState = newState;	
		currentState->enter(*this);		
	}
}

/*
* Game run loop.
*/
void Game::run(void)
{	
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)
	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) 
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;			
		}		
		currentState->draw(hWindow, *this);
		currentState->update(*this);		
	}
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();    
}


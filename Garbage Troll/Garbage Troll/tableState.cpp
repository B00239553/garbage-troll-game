//(c) Michael O'Neil 2015

#include "game.h"
#include "tableState.h"
#include <ctime>

/*
* Constructor.
*/
tableState::tableState(void)
{
}

/*
* Variable Constructor. Takes in variables so object is only loaded once.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for state.
*/
tableState::tableState(GLuint mesh, GLuint count, GLuint shader)
{	
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = shader;	
	tableUpdated = false;
}

/*
* Deconstructor.
*/
tableState::~tableState(void)
{
}

/*
* Create text textures.
* Loads current high score table.
* @param - Game - the context of the game.
*/
void tableState::init(Game &context)
{			
	std::string str;
	int score;
	std::ifstream in_file ("Highscores.txt");
	
	if(in_file.is_open())	
	{
		for(int i = 0; i < MAX; ++i)
		{	
			in_file >> str;
			names[i] = str;
			in_file >> score;			
			highScores[i] = score;
		}
	}
	else
		std::cout << "File could not open." << std::endl;
	in_file.close();	
	textures[0] = context.textToTexture("High Score Table", glm::vec3(255, 100, 0));
	std::stringstream strStream;
	for(int i = 0; i < MAX; i++)
	{
		strStream << names[i] << "         " << highScores[i];
		textures[i+1] = context.textToTexture(strStream.str().c_str(), glm::vec3(255, 100, 0));
		strStream.str(std::string());
	}	
	textures[11] = context.textToTexture("Back (B)", glm::vec3(255, 100, 0));
}

/*
* Updates table if score high enough.
* @param - Game - the context of the game.
*/
void tableState::enter(Game &context)
{		
	if(context.getScore() > 0 && context.getInGame() == false)
	{		
		int i = 0;		
		while(i < MAX && tableUpdated == false)
		{
			if(context.getScore() > highScores[i])
			{
				int pos = i;
				for(int j = MAX - 1; j > pos; j--)
				{
					names[j] = names[j - 1];
					highScores[j] = highScores[j - 1];
				}
				names[pos] = context.getName();
				highScores[pos] = context.getScore();				
				tableUpdated = true;
			}
			else
				i++;
		}	
		context.setScore(0);
	}
}

/*
* Displays the table.
* @param - SDL_Window - window to draw to.
* @param - Game - game context.
*/
void tableState::draw(SDL_Window * window, Game &context)
{		
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(glm::mat4(1.0f)));
	glDepthMask(GL_FALSE); // make sure depth test is off		
	glUseProgram(shaderProgram);

	glBindTexture(GL_TEXTURE_2D, textures[0]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.9f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.75f, 0.20f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();		
		
	glBindTexture(GL_TEXTURE_2D, textures[1]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.65f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();			
		
	glBindTexture(GL_TEXTURE_2D, textures[2]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.53f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[3]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.41f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[4]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.29f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[5]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.17f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[6]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.05f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[7]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.07f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[8]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.19f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[9]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.31f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[10]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.43f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();	

	glBindTexture(GL_TEXTURE_2D, textures[11]);	
	mvStack.push(glm::mat4(1.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.7f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.3f, 0.1f, 0.0f));	
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
	mvStack.pop();			
	

	glDepthMask(GL_TRUE);	
	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* Updates table textures if a new score has been entered.
* @param - Game - the context of the game.
*/
void tableState::update(Game &context)
{		
	Uint8 *keys = SDL_GetKeyboardState(NULL);	
	if ( keys[SDL_SCANCODE_B] )
		context.setState(context.getMainMenuState());	

	if(tableUpdated == true)
	{
		std::stringstream strStream;
		for(int i = 0; i < MAX; i++)
		{
			strStream << names[i] << "         " << highScores[i];
			textures[i+1] = context.textToTexture(strStream.str().c_str(), glm::vec3(255, 100, 0));
			strStream.str(std::string());
		}	
	}
}

/*
* Saves Table to file.
* @param - Game - the context of the game.
*/
void tableState::exit(Game &context)
{		
	std::ofstream myFile ("Highscores.txt");
	if(myFile.is_open())
	{		
		for(int i = 0; i < MAX; ++i)
		{
			myFile << names[i];			
			myFile << "\n";
			myFile << highScores[i];
			myFile << "\n";
		}	
		myFile.close();
	}
	else
		std::cout << "Unable to open file" << std::endl;	
	tableUpdated = false;
}
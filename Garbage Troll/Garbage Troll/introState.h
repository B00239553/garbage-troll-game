//(c) Michael O'Neil 2015

#ifndef INTROSTATE_H   
#define INTROSTATE_H   

/*
* Intro State - Used at game load only, player can bypass by pressing
* space or return, will automatically change after 6 seconds.
*/
class introState : public gameState {
private:	
	GLuint textures[3];	
public:			
	introState(void);	
	introState(GLuint mesh, GLuint count, GLuint shader);
	~introState(void);
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void update(Game &context);	
	void enter(Game &context);
	void exit(Game &context);		
};

#endif 
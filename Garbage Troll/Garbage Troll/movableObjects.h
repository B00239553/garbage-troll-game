//(c) Michael O'Neil 2015

#ifndef MOVEABLEOBJECTS_H  
#define MOVEABLEOBJECTS_H

#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "object.h"
#include <stack>
#include "AABB.h"

/*
* Class to create, draw movable objects in the world.
*/
class movableObjects
{
private:
	static const int MAX_OBJECTS = 15;
	object * worldObjects[MAX_OBJECTS];
	int objsHeld;
	GLuint meshObject;
	GLuint meshIndexCount;
	GLuint texture;
	AABB * boxes[MAX_OBJECTS];
	bool AABBset;
	std::vector<GLfloat> verts;	
	GLfloat spin;
	GLuint loadBitmap(char *fname);		
public:
	movableObjects(void);
	movableObjects(std::vector<GLfloat> v, GLuint mesh, GLuint count);
	~movableObjects(void);
	void init(void);
	int getNumObjects(void) {return objsHeld;}
	object *getObject(int obj) {return worldObjects[obj];}
	AABB *getAABB(int box) {return boxes[box];}
	void draw(std::stack<glm::mat4> mvStack, GLuint shader);	
	void deleteObject(object* thisObject);
};

#endif
//(c) Michael O'Neil 2015

#include "object.h"

/*
* Constructor.
*/
object::object(void)
{
}

/*
* Variable Constructor.
* @param - std::string - name of object.
* @param - std::string - type of object.
* @param - GLuint - texture of object.
* @param - glm::vec3 - vec to translate position.
* @param - glm::vec3 - vec to rotate object.
* @param - float - angle of rotation.
* @param - glm::vec3 - vec to scale object.
* @param - GLuint - mesh for object.
* @param - int - index count for mesh.
*/
object::object(std::string mName, std::string mType, GLuint mTex, glm::vec3 mTranslate, glm::vec3 mRotate, float mRotation, glm::vec3 mScale, GLuint mMeshObject, int mMeshCount)
{
	name = mName;	
	type = mType;
	texture = mTex;
	translate = mTranslate;
	rotate = mRotate;
	rotation = mRotation;
	scale = mScale;
	meshObject = mMeshObject;
	meshIndexCount = mMeshCount;	
}

/*
* Deconstructor.
*/
object::~object(void)
{
}
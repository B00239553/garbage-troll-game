//(c) Michael O'Neil 2015

#include "movableObjects.h"

/*
* Constructor.
*/
movableObjects::movableObjects(void)
{
	AABBset = false;
}

/*
* Variable Constructor. Takes in variables so object is only loaded once.
* @param - STD::vector<GLfloat> - verts of cube, used for AABB creation.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
*/
movableObjects::movableObjects(std::vector<GLfloat> v, GLuint mesh, GLuint count)
{
	verts = v;
	meshObject = mesh;
	meshIndexCount = count;
	objsHeld = 0;
	AABBset = false;
	spin = 0;
}

/*
* Deconstructor.
*/
movableObjects::~movableObjects(void)
{
	for(int i = 0; i < objsHeld; i++)
	{
		delete worldObjects[i];
		worldObjects[i] = NULL;
	}
	for(int j = 0; j < objsHeld; j++)
	{
		delete boxes[j];
		boxes[j] = 0;
	}
}

/*
* Will load all bitmap files and create all world objects.
*/
void movableObjects::init(void)
{		
	texture = loadBitmap("bin.bmp");	
	worldObjects[0] = new object("bin1", "Bin", texture, glm::vec3(-2.5f, 0.45f, -4.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(0.5, 1.5f, 0.5f), meshObject, meshIndexCount);
	objsHeld++;
	worldObjects[1] = new object("bin2", "Bin", texture, glm::vec3(2.5f,  0.45f, -4.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(0.5, 1.5f, 0.5f), meshObject, meshIndexCount);
	objsHeld++;
	worldObjects[2] = new object("bin3", "Bin", texture, glm::vec3(-2.5f, 0.45f, -16.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(0.5, 1.5f, 0.5f), meshObject, meshIndexCount);
	objsHeld++;
	worldObjects[3] = new object("bin4", "Bin", texture, glm::vec3(2.5f, 0.45f, -16.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(0.5, 1.5f, 0.5f), meshObject, meshIndexCount);
	objsHeld++;
	worldObjects[4] = new object("bin5", "Bin", texture, glm::vec3(-11.5f, 0.45f, -15.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(0.5, 1.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	texture = loadBitmap("garbage.bmp");
	worldObjects[5] = new object("garbage1", "Garbage", texture, glm::vec3(6.65f, 0.5f, -11.9f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[6] = new object("garbage2", "Garbage", texture, glm::vec3(3.0f, 1.0f, -22.6f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[7] = new object("garbage3", "Garbage", texture, glm::vec3(-3.0f, 1.5f, -22.84f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[8] = new object("garbage4", "Garbage", texture, glm::vec3(0.01f, 2.0f, -12.22f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[9] = new object("garbage5", "Garbage", texture, glm::vec3(-5.4f, 0.5f, -14.84f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[10] = new object("garbage6", "Garbage", texture, glm::vec3(-13.82f, 1.0f, -14.85f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[11] = new object("garbage7", "Garbage", texture, glm::vec3(-13.94f, 1.5f, -1.02f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[12] = new object("garbage8", "Garbage", texture, glm::vec3(-6.07f, 2.0f, -3.32f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[13] = new object("garbage9", "Garbage", texture, glm::vec3(-9.9f, 0.5f, -8.4f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
	worldObjects[14] = new object("garbage10", "Garbage", texture, glm::vec3(3.01f, 1.0f, -1.17f), glm::vec3(0.0f, 0.0f, 1.0f), spin, glm::vec3(0.5, 0.5f, 0.5f), meshObject, meshIndexCount);	
	objsHeld++;
}

/*
* Load bitmap file from name taken in.
* @param - char * - name of bitmap file.
* @return - GLuint - returns texture created from bitmap image.
*/
GLuint movableObjects::loadBitmap(char *fname)
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	// load file - using core SDL library
 	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 

	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID;	// return value of texture ID
}

/*
* Draws all movable objects.
* Creates AABBs in first call for collisions.
* @param - std::stack<glm::mat4> - the current modelview matrix.
* @param - GLuint - current shader to be used.
*/
void movableObjects::draw(std::stack<glm::mat4> mvStack, GLuint shader)	
{
	rt3d::materialStruct materialEnvironment = {
		{0.4f, 0.2f, 0.2f, 1.0f}, // ambient
		{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse 
		{1.0f, 0.8f, 0.8f, 1.0f}, // specular
		2.0f  // shininess
	};

	rt3d::setMaterial(shader, materialEnvironment);

	//Spins all garbage objects
	spin+= 1;
	for(int i = 5; i < objsHeld; i++)
		worldObjects[i]->setRotation(spin);
	
	for(int i = 0; i < objsHeld; i++)
	{
		glUseProgram(shader);
		mvStack.push(mvStack.top());
		glBindTexture(GL_TEXTURE_2D, worldObjects[i]->getTex());
		mvStack.top() = glm::translate(mvStack.top(), worldObjects[i]->getTranslate());		
		mvStack.top() = glm::rotate(mvStack.top(), worldObjects[i]->getRotation(), worldObjects[i]->getRotate());		
		mvStack.top() = glm::scale(mvStack.top(), worldObjects[i]->getScale());		
		rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(worldObjects[i]->getMeshObject(),worldObjects[i]->getMeshIndexCount(),GL_TRIANGLES);
		
		//Sets AABB for world objects, only set once as bool will be true after first call.
		if(AABBset == false)
		{				
			std::vector<GLfloat> tmpVerts = verts;
			for(int i = 0; i < tmpVerts.size(); i=i+3)
			{
				glm::vec4 tempVec = mvStack.top() * glm::vec4(tmpVerts[i], tmpVerts[i+1], tmpVerts[i+2], 1.0);
				tmpVerts[i] = tempVec.x;
				tmpVerts[i+1] = tempVec.y;
				tmpVerts[i+2] = tempVec.z;	
			}
			boxes[i] = new AABB(worldObjects[i]->getName());
			boxes[i]->setAABB(tmpVerts, tmpVerts.size());	
			//Some tweaking required as not rotated at origin.
			boxes[i]->updateAABB(glm::vec3(boxes[i]->getMax().x - boxes[i]->getLength()/2 + 0.9, boxes[i]->getMax().y - boxes[i]->getHeight()/2 + 1.0, boxes[i]->getMax().z - boxes[i]->getDepth()/2));
		}		
		boxes[i]->updateAABB(worldObjects[i]->getTranslate());
		mvStack.pop();
	}	
	if(AABBset == false)
		AABBset = true;
}

/*
* Deletes the Object taken in the parameter, it will also move
* elements in the array and reduce the current objects held. 
* @param - object* - the object to be deleted.
*/
void movableObjects::deleteObject(object* thisObject)
{	
	int index = 0;	
	while(thisObject != worldObjects[index])
	{
		worldObjects[index++];		
	}	
	delete thisObject;	
	thisObject = NULL;
		
	for(;index < (objsHeld - 1); index++)
	{
		worldObjects[index] = worldObjects[index+1];
	}
	objsHeld--;	
}
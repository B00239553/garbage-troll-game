//(c) Michael O'Neil 2015

#include "troll.h"
#define DEG_TO_RAD 0.017453293

/*
* Constructor.
*/
troll::troll(void)
{	
}

/*
* Variable Constructor.
* @param - std::string - player name.
*/
troll::troll(std::string mName)
{
	name = mName;
	pos = glm::vec3(1.0f, 0.5f, -3.0f);
	itemsHeld = 0;
	radius = 0.0f;
	rad = 0.0f;	
	currentAnim = 0;
	AABBset = false;		
}

/*
* Deconstructor.
*/
troll::~troll(void)
{
	delete box;
	box = NULL;
}

/*
* Loads texture and creates troll mesh object.
*/
void troll::init(void)
{
	texture = loadBitmap("hobgoblin.bmp");
	meshObject = tmpModel.ReadMD2Model("tris.MD2");
}

/*
* Load bitmap file from name taken in.
* @param - char * - name of bitmap file.
* @return - GLuint - returns texture created from bitmap image.
*/
GLuint troll::loadBitmap(char * fname)
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	// load file - using core SDL library
 	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 

	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) 
	{
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else
	{
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID;	// return value of texture ID
}

/*
* Required to move the troll forwards and backwards.
* @param - glm::vec3 - the position of the troll.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 troll::moveForward(glm::vec3 cam, GLfloat angle, GLfloat d) {
	return glm::vec3(cam.x + d*std::sin(angle*DEG_TO_RAD), cam.y, cam.z - d*std::cos(angle*DEG_TO_RAD));
}

/*
* Required to move the troll right and left.
* @param - glm::vec3 - the position of the troll.
* @param - GLfloat - the angle of rotation.
* @param - GLfloat - the distance to be moved.
* @return = glm::vec3 - the new position.
*/
glm::vec3 troll::moveRight(glm::vec3 pos, GLfloat angle, GLfloat d) {	
	return glm::vec3(pos.x + d*std::cos(angle*DEG_TO_RAD), pos.y, pos.z + d*std::sin(angle*DEG_TO_RAD)); 
}

/*
* Moves the troll forward, corrects radius for drawing and
* changes to the correct animation.
* @param - GLfloat - the current radius to allow correct movement.
*/
void troll::moveForward(GLfloat r)
{
	pos = moveForward(pos, r, 0.1);
	radius = rad;
	currentAnim = 1;	
}

/*
* Moves the troll back, corrects radius for drawing and
* changes to the correct animation.
* @param - GLfloat - the current radius to allow correct movement.
*/
void troll::moveBack(GLfloat r)
{
	pos = moveForward(pos, r, -0.1);
	radius = rad - 180;
	currentAnim = 1;
}

/*
* Moves the troll right, corrects radius for drawing and
* changes to the correct animation.
* @param - GLfloat - the current radius to allow correct movement.
*/
void troll::moveRight(GLfloat r)
{
	pos = moveRight(pos, r, 0.1);
	radius = rad - 270;
	currentAnim = 1;	
}

/*
* Moves the troll left, corrects radius for drawing and
* changes to the correct animation.
* @param - GLfloat - the current radius to allow correct movement.
*/
void troll::moveLeft(GLfloat r)
{
	pos = moveRight(pos, r, -0.1);
	radius = rad - 90;
	currentAnim = 1;
}

/*
* Makes troll jump, sets new animation.
*/
void troll::jump(void)
{
	pos.y = 0.9f;
	box->updateAABB(getPos());
	currentAnim = 6;
}

/*
* Sets a temp radius that is used when the player moves,
* this allows the rotation of the troll to remain correct
* depending what direction is moving.
* @param - GLfloat - the current radius.
*/
void troll::setRad(GLfloat r)
{
	rad = r;
}

/*
* Changes the animation.
* @param - int - the animation to be set.
*/
void troll::setAnimation(int anim)
{
	currentAnim = anim;
}

/*
* Sets trolls position, used if collision occurred.
* @param - glm::vec3 - the new position to be set.
*/
void troll::setPos(glm::vec3 position)
{
	pos = position;
}

/*
* Draws the troll.
* Creates AABB in first call to draw, further calls AABB
* will be moved through update function.
* @param - std::stack<glm::mat4> - the current modelview.
* @param - GLuint - the shader to be used.
*/
void troll::draw(std::stack<glm::mat4> mvStack, GLuint shader)
{
	rt3d::materialStruct materialPlayer = {
	{0.4f, 0.4f, 1.0f, 1.0f}, // ambient
	{0.8f, 0.8f, 1.0f, 1.0f}, // diffuse
	{0.8f, 0.8f, 0.8f, 1.0f}, // specular
	1.0f  // shininess
	};

	tmpModel.Animate(currentAnim,0.1);
	rt3d::updateMesh(meshObject,RT3D_VERTEX,tmpModel.getAnimVerts(),tmpModel.getVertDataSize());
		
	//draw the troll
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, texture);
	rt3d::materialStruct tmpMaterial = materialPlayer;
	rt3d::setMaterial(shader, tmpMaterial);
	mvStack.push(mvStack.top());
	//allows player to move based on player vector position
	mvStack.top() = glm::translate(mvStack.top(), pos);
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	//allows player to rotate based on the player radius
	mvStack.top() = glm::rotate(mvStack.top(), -radius, glm::vec3(0.0f, 0.0f, 1.0f));
	GLfloat scale = 0.75f*0.05f;
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale, scale, scale));	
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(meshObject, tmpModel.getVertDataCount(), GL_TRIANGLES);			
	glCullFace(GL_BACK);
	//Sets AABB for troll for game load
	if(AABBset == false)
	{
		GLfloat * modelVerts = tmpModel.getAnimVerts();
		for(int i = 0; i < tmpModel.getVertDataSize(); i++)
		{
			verts.push_back(modelVerts[i]);
		}		
		for(int i = 0; i < tmpModel.getVertDataSize(); i=i+3)
		{
			glm::vec4 tempVec = mvStack.top() * glm::vec4(verts[i], verts[i+1], verts[i+2], 1.0);
			verts[i] = tempVec.x;
			verts[i+1] = tempVec.y;
			verts[i+2] = tempVec.z;	
		}
		box = new AABB(name);
		box->setAABB(verts, tmpModel.getVertDataSize());		
		AABBset = true;
	}	
	mvStack.pop();			
}
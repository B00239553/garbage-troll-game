//(c) Michael O'Neil 2015

#include "rt3d.h"
#include "md2model.h"
#include <stack>
#include <vector>
#include "AABB.h"

#ifndef TROLL_H  
#define TROLL_H

/*
* Class to create, draw troll in the world.
* Also has functions to move troll, jump, set 
* radius for movement and change animation of the troll.
*/
class troll
{
private:
	std::string name;
	glm::vec3 pos;
	int itemsHeld;
	GLfloat radius;
	GLfloat rad;	
	GLuint texture;
	GLuint meshObject;
	md2model tmpModel; 
	int currentAnim;
	AABB * box;
	bool AABBset;
	std::vector<GLfloat> verts;		
	GLuint loadBitmap(char *fname);	
	glm::vec3 moveForward(glm::vec3 cam, GLfloat angle, GLfloat d);
	glm::vec3 moveRight(glm::vec3 pos, GLfloat angle, GLfloat d);
public:
	troll(void);
	~troll(void);
	troll(std::string mName);
	void init(void);
	void draw(std::stack<glm::mat4> mvStack, GLuint shader);
	void moveForward(GLfloat r);
	void moveBack(GLfloat r);
	void moveRight(GLfloat r);
	void moveLeft(GLfloat r);
	void jump(void);
	std::string getName() {return name;}
	glm::vec3 getPos(void) {return pos;}
	int getItemsHeld(void) {return itemsHeld;}
	void setPos(glm::vec3 position);
	void setItemsHeld(int item) {itemsHeld = item;}
	AABB *getAABB(void) {return box;}
	void setAnimation(int anim);
	void setRad(GLfloat r);		
};

#endif
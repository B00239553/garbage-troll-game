//(c) Michael O'Neil 2015

#include "rt3d.h"
#include <stack>

using namespace std;

#ifndef SKYBOX_H  
#define SKYBOX_H

/*
* Class to create, draw skybox for the world.
* Is drawn without any details from main draw, to be able to rotate
* requires float to be taken in to be able to rotate the skybox.
*/
class skyBox {
private:	
	static const GLint VERTEX_COUNT = 18;
	static const GLint INDEX_COUNT = 6;
	static const GLint TEX_COORD_COUNT = 12;
	static const GLint TEX_COUNT = 5;
	GLfloat vertices[VERTEX_COUNT];
	GLfloat vertexTexCoord[TEX_COORD_COUNT];
	GLuint skyBoxIndices[INDEX_COUNT];
	GLuint textures[TEX_COUNT];
	std::stack<glm::mat4> mvStack; 	
	GLuint skyBoxProgram; 	
	GLuint meshObject;
	GLfloat radius;
	GLuint loadBitmap(char *fname);	
public:
	skyBox(GLuint shader);	
	~skyBox(void);
	void init(void);				
	void draw(void);
	void setRadius(GLfloat rad);			
};

#endif
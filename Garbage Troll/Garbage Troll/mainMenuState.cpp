//(c) Michael O'Neil 2015

#include "game.h"
#include "mainMenuState.h"

/*
* Constructor.
*/
mainMenuState::mainMenuState(void)
{
}

/*
* Variable Constructor. Takes in variables so object is only loaded once.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
* @param - GLuint - shader program for state.
*/
mainMenuState::mainMenuState(GLuint mesh, GLuint count, GLuint shader)
{	
	meshObject = mesh;
	meshIndexCount = count;	
	shaderProgram = shader;
	showInstructions = false;
	newGame = false;
	loseProgress = false;
	playerName = "_____________";
}

/*
* Deconstructor.
*/
mainMenuState::~mainMenuState(void)
{
}

/*
* Create text textures.
* @param - Game - the context of the game.
*/
void mainMenuState::init(Game &context)
{			
	textures[0] = context.textToTexture("Main Menu", glm::vec3(255, 100, 0));	
	textures[1] = context.textToTexture("    New Game (N)    ", glm::vec3(255, 100, 0));
	textures[2] = context.textToTexture("  Instructions (I)  ", glm::vec3(255, 100, 0));
	textures[3] = context.textToTexture("High Score Table (H)", glm::vec3(255, 100, 0));
	textures[4] = context.textToTexture("    Credits (R)     ", glm::vec3(255, 100, 0));
	textures[5] = context.textToTexture("     Quit (Esc)     ", glm::vec3(255, 100, 0));
	textures[6] = context.textToTexture("    Continue (C)    ", glm::vec3(255, 100, 0));
	textures[7] = context.textToTexture("Instructions", glm::vec3(255, 100, 0));
	textures[8] = context.textToTexture("Controls", glm::vec3(255, 100, 0));	
	textures[9] = context.textToTexture("W - Move Forward                 ", glm::vec3(255, 100, 0));
	textures[10] = context.textToTexture("S - Move Back                   ", glm::vec3(255, 100, 0));
	textures[11] = context.textToTexture("A - Move Left                   ", glm::vec3(255, 100, 0));
	textures[12] = context.textToTexture("D - Move Right                  ", glm::vec3(255, 100, 0));
	textures[13] = context.textToTexture("Left Arrow - Rotate Camera Right", glm::vec3(255, 100, 0));
	textures[14] = context.textToTexture("Right Arrow - Rotate Camera Left", glm::vec3(255, 100, 0));
	textures[15] = context.textToTexture("Objective", glm::vec3(255, 100, 0));
	textures[16] = context.textToTexture("Collect all of the garbage as fast as possible. Only two items can be held at a time.", glm::vec3(255, 100, 0));
	textures[17] = context.textToTexture("Drop garbage into a bin by bumping into one.                                         ", glm::vec3(255, 100, 0));
	textures[18] = context.textToTexture("Space - Jump           ", glm::vec3(255, 100, 0));
	textures[19] = context.textToTexture("Back (B)", glm::vec3(255, 100, 0));
	textures[20] = context.textToTexture("Enter name and press return", glm::vec3(255, 100, 0));
	textures[21] = context.textToTexture(playerName.c_str(), glm::vec3(255, 100, 0));
	textures[22] = context.textToTexture("Current progress will be lost!", glm::vec3(255, 100, 0));
	textures[23] = context.textToTexture("Are you sure? (Y/N)", glm::vec3(255, 100, 0));
}

/*
* Resets name and name texture.
* @param - Game - the context of the game.
*/
void mainMenuState::enter(Game &context)
{		
	playerName = "_____________";
	textures[21] = context.textToTexture(playerName.c_str(), glm::vec3(255, 100, 0));	
}

/*
* Displays the main menu. Depending on what key pressed will draw
* different sections if still in main menu state.
* @param - SDL_Window - window to draw to.
* @param - Game - game context.
*/
void mainMenuState::draw(SDL_Window * window, Game &context)
{		
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(glm::mat4(1.0f)));
	glDepthMask(GL_FALSE); // make sure depth test is off		
	glUseProgram(shaderProgram);

	if(showInstructions == false && newGame == false) //main menu
	{
		glBindTexture(GL_TEXTURE_2D, textures[0]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.9f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.75f, 0.20f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();		
		
		glBindTexture(GL_TEXTURE_2D, textures[1]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.25f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();			
	
		glBindTexture(GL_TEXTURE_2D, textures[2]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.1f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();		
		
		glBindTexture(GL_TEXTURE_2D, textures[3]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.05f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();		
	
		glBindTexture(GL_TEXTURE_2D, textures[4]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.2f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();		
	
		glBindTexture(GL_TEXTURE_2D, textures[5]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.35f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();		
	
		if(context.getInGame() == true) //only displays if game is paused
		{
			glBindTexture(GL_TEXTURE_2D, textures[6]);	
			mvStack.push(glm::mat4(1.0f));
			mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.4f, 0.0f));
			mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.6f, 0.15f, 0.0f));	
			mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
			rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
			rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
			mvStack.pop();	
		}		
	}
	else if(showInstructions == true) //shows instructions
	{
		glBindTexture(GL_TEXTURE_2D, textures[7]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.9f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.75f, 0.20f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[8]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.8f, 0.7f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.3f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[9]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.65f, 0.55f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.55f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[10]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.65f, 0.45f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.55f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[11]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.65f, 0.35f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.55f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[12]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.65f, 0.25f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.55f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[18]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.65f, 0.15f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.55f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[13]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.65f, 0.05f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.55f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[14]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.65f, -0.05f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.55f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[15]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-0.8f, -0.2f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.3f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[16]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.35f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(1.9f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[17]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.45f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(1.9f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();		

		glBindTexture(GL_TEXTURE_2D, textures[19]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.7f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.3f, 0.1f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	
	}
	else if(newGame == true && loseProgress == true) //new game has been pressed, make player enter name.
	{
		glBindTexture(GL_TEXTURE_2D, textures[20]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.4f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.7f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	

		glBindTexture(GL_TEXTURE_2D, textures[21]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.2f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.5f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	
	}
	else if(newGame == true && loseProgress == false) //check if wish to lose progress as current game is held
	{
		glBindTexture(GL_TEXTURE_2D, textures[22]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.3f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.8f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();

		glBindTexture(GL_TEXTURE_2D, textures[23]);	
		mvStack.push(glm::mat4(1.0f));
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.2f, 0.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.5f, 0.15f, 0.0f));	
		mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
		mvStack.pop();	
	}
	
	glDepthMask(GL_TRUE);	
	SDL_GL_SwapWindow(window); // swap buffers
}

/*
* Handles any events that can be used in this state.
* @param - Game - the context of the game.
*/
void mainMenuState::update(Game &context)
{		
	SDL_Event sdlEvent;
	SDL_PollEvent(&sdlEvent);
	Uint8 *keys = SDL_GetKeyboardState(NULL);
	if(showInstructions == false && newGame == false) //main menu interaction
	{
		if (sdlEvent.type == SDL_KEYDOWN)
		{		
			switch( sdlEvent.key.keysym.sym )
			{	
				case 'n': case 'N':			  
					newGame = true;
					if(context.getInGame() == true) //if game held			 
						loseProgress = false; //asks question if sure wish to lose progress
					else
						loseProgress = true; //game not held, can lose progress as none					
				default:
					break;
			}
		}			
		if ( keys[SDL_SCANCODE_I])
			showInstructions = true;
		if ( keys[SDL_SCANCODE_H])
			context.setState(context.getTableState());
		if ( keys[SDL_SCANCODE_R])
			context.setState(context.getCreditsState());
		if ( keys[SDL_SCANCODE_C])
			context.setState(context.getPlayState());
		if ( keys[SDL_SCANCODE_ESCAPE])
		{
			if(context.getInGame() == true) 
					context.getPlayState()->exit(context); //delete in game objects as game held
			context.setRunning(false);
		}
	}
	else if(showInstructions == true) //instruction interactions	
	{
		if ( keys[SDL_SCANCODE_B])
			showInstructions = false;	
	}
	else if(newGame == true && loseProgress == true) //used for entering name after new game has been pressed
	{												 
		if (sdlEvent.type == SDL_KEYDOWN)
		{		
			switch( sdlEvent.key.keysym.sym )
			{	
				case SDLK_BACKSPACE:
					if(playerName.size() > 0 && playerName != "_____________")			
						playerName.resize(playerName.size()-1);	
					if(playerName.size() == 0)				
						playerName = "_____________";
					break;				
				case 'a': case 'A':
					if(playerName == "_____________")
						playerName = "A";
					else
						playerName += "a";
					break;
				case 'b': case 'B':
					if(playerName == "_____________")
						playerName = "B";
					else
						playerName += "b";
					break;
				case 'c': case 'C':
					if(playerName == "_____________")
						playerName = "C";
					else
						playerName += "c";
					break;
				case 'd': case 'D':
					if(playerName == "_____________")
						playerName = "D";
					else
						playerName += "d";
					break;
				case 'e': case 'E':
					if(playerName == "_____________")
						playerName = "E";
					else
						playerName += "e";
					break;
				case 'f': case 'F':
					if(playerName == "_____________")
						playerName = "F";
					else
						playerName += "f";
					break;
				case 'g': case 'G':
					if(playerName == "_____________")
						playerName = "G";
					else
						playerName += "g";
					break;
				case 'h': case 'H':
					if(playerName == "_____________")
						playerName = "H";
					else
						playerName += "h";
					break;
				case 'i': case 'I':
					if(playerName == "_____________")
						playerName = "I";
					else
						playerName += "i";
					break;
				case 'j': case 'J':
					if(playerName == "_____________")
						playerName = "J";
					else
						playerName += "j";
					break;
				case 'k': case 'K':
					if(playerName == "_____________")
						playerName = "K";
					else
						playerName += "k";
					break;
				case 'l': case 'L':
					if(playerName == "_____________")
						playerName = "L";
					else
						playerName += "l";
					break;
				case 'm': case 'M':
					if(playerName == "_____________")
						playerName = "M";
					else
						playerName += "m";
					break;
				case 'n': case 'N':
					if(playerName == "_____________")
						playerName = "N";
					else
						playerName += "n";
					break;
				case 'o': case 'O':
					if(playerName == "_____________")
						playerName = "O";
					else
						playerName += "o";
					break;
				case 'p': case 'P':
					if(playerName == "_____________")
						playerName = "P";
					else
						playerName += "p";
					break;
				case 'q': case 'Q':
					if(playerName == "_____________")
						playerName = "Q";
					else
						playerName += "q";
					break;
				case 'r': case 'R':
					if(playerName == "_____________")
						playerName = "R";
					else
						playerName += "r";
					break;
				case 's': case 'S':
					if(playerName == "_____________")
						playerName = "S";
					else
						playerName += "s";
					break;
				case 't': case 'T':
					if(playerName == "_____________")
						playerName = "T";
					else
						playerName += "t";
					break;
				case 'u': case 'U':
					if(playerName == "_____________")
						playerName = "U";
					else
						playerName += "u";
					break;
				case 'v': case 'V':
					if(playerName == "_____________")
						playerName = "V";
					else
						playerName += "v";
					break;
				case 'w': case 'W':
					if(playerName == "_____________")
						playerName = "W";
					else
						playerName += "w";
					break;
				case 'x': case 'X':
					if(playerName == "_____________")
						playerName = "X";
					else
						playerName += "x";
					break;
				case 'y': case 'Y':
					if(playerName == "_____________")
						playerName = "Y";
					else
						playerName += "y";
					break;
				case 'z': case 'Z':
					if(playerName == "_____________")
						playerName = "Z";
					else
						playerName += "z";
					break;
				case SDLK_RETURN :
					if ( newGame == true && playerName != "_____________")
						context.setState(context.getPlayState());
					break;			
				default:
					break;
								
			}
			//updates texture for player name being entered
			textures[21] = context.textToTexture(playerName.c_str(), glm::vec3(255, 100, 0));
		}		
	}
	else if(newGame == true && loseProgress == false) //game is held, interaction to check if wish to lose it
	{
		if (sdlEvent.type == SDL_KEYDOWN)
		{		
			switch( sdlEvent.key.keysym.sym )
			{	
				case 'y': case 'Y':			  //lose current game
					loseProgress = true;
					context.setInGame(false); //game now not held
					context.getPlayState()->exit(context); //delete in game objects
					break;				
				case 'n': case 'N':			  //do not lose current game, will return to main menu
					newGame = false;
					break;
				default:
					break;
			}
		}		
	}
}

/*
* Sets new game as false so will display main menu next
* time is entered.
* @param - Game - the context of the game.
*/
void mainMenuState::exit(Game &context)
{		
	if(newGame == true)//sets player name for other states
	{
		context.setScore(0);
		context.setName(playerName);
	}
	newGame = false;
}
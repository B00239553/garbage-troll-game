//(c) Michael O'Neil 2015

#include "immovableObjects.h"

/*
* Constructor.
*/
immovableObjects::immovableObjects(void)
{
	AABBset = false;
}

/*
* Variable Constructor. Takes in variables so object is only loaded once.
* @param - STD::vector<GLfloat> - verts of cube, used for AABB creation.
* @param - GLuint - mesh of cube.
* @param - GLuint - count of indices.
*/
immovableObjects::immovableObjects(std::vector<GLfloat> v, GLuint mesh, GLuint count)
{
	verts = v;
	meshObject = mesh;
	meshIndexCount = count;
	AABBset = false;
}

/*
* Deconstructor.
*/
immovableObjects::~immovableObjects(void)
{	
	for(int i = 0; i < MAX_OBJECTS; i++)
	{
		delete worldObjects[i];
		worldObjects[i] = NULL;
	}	
	if(AABBset == true)
		for(int j = 0; j < MAX_OBJECTS; j++)
		{
			delete boxes[j];
			boxes[j] = NULL;
		}
}

/*
* Will load all bitmap files and create all world objects.
*/
void immovableObjects::init(void)
{		
	texture = loadBitmap("road.bmp");
	worldObjects[0] = new object("Main Road", "Road", texture, glm::vec3(0.0f, -0.6f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(4.0f, 0.2f, -24.0f), meshObject, meshIndexCount);
	worldObjects[1] = new object("Left Road", "Road", texture, glm::vec3(-15.0f, -0.6f, -12.0f), glm::vec3(0.0f, 1.0f, 0.0f), 270.0f, glm::vec3(4.0f, 0.2f, -13.0f), meshObject, meshIndexCount);
	texture = loadBitmap("pavement.bmp");
	worldObjects[2] = new object("Ground", "Ground", texture, glm::vec3(3.0f, -0.4f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(2.0f, 0.2f, -24.0f), meshObject, meshIndexCount);
	worldObjects[3] = new object("Ground", "Ground", texture, glm::vec3(-3.0f, -0.4f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(2.0f, 0.2f, -10.0f), meshObject, meshIndexCount);
	worldObjects[4] = new object("Ground", "Ground", texture, glm::vec3(-3.0f, -0.4f, -14.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(2.0f, 0.2f, -10.0f), meshObject, meshIndexCount);
	worldObjects[5] = new object("Ground", "Ground", texture, glm::vec3(-15.0f, -0.4f, -9.0f), glm::vec3(0.0f, 1.0f, 0.0f), 270.0f, glm::vec3(2.0f, 0.2f, -11.0f), meshObject, meshIndexCount);
	worldObjects[6] = new object("Ground", "Ground", texture, glm::vec3(-15.0f, -0.4f, -15.0f), glm::vec3(0.0f, 1.0f, 0.0f), 270.0f, glm::vec3(2.0f, 0.2f, -11.0f), meshObject, meshIndexCount);
	texture = loadBitmap("building pavement.bmp");
	worldObjects[7] = new object("Ground", "Ground", texture, glm::vec3(6.0f, -0.4f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(4.0f, 0.2f, -24.0f), meshObject, meshIndexCount);
	worldObjects[8] = new object("Ground", "Ground", texture, glm::vec3(-9.5f, -0.4f, -16.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(11.0f, 0.2f, -8.0f), meshObject, meshIndexCount);
	texture = loadBitmap("Grass.bmp");
	worldObjects[9] = new object("Ground", "Ground", texture, glm::vec3(-9.5f, -0.4f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(11.0f, 0.2f, -8.0f), meshObject, meshIndexCount);
	texture = loadBitmap("wall.bmp");	
	worldObjects[10] = new object("Wall front", "Building", texture, glm::vec3(-3.5f, 0.4f, -24.5f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(24.0f, 2.0f, 0.5f), meshObject, meshIndexCount);
	worldObjects[11] = new object("Wall back", "Building", texture, glm::vec3(-3.5f, 0.4f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 180.0f, glm::vec3(24.0f, 2.0f, -0.5f), meshObject, meshIndexCount);
	worldObjects[12] = new object("Wall right", "Building", texture, glm::vec3(8.0f, 0.4f, -12.0), glm::vec3(0.0f, 1.0f, 0.0f), 90.0f, glm::vec3(24.0f, 2.0f, 0.5f), meshObject, meshIndexCount);
	worldObjects[13] = new object("Wall left", "Building", texture, glm::vec3(-15.5f, 0.4f, -12.0f), glm::vec3(0.0f, 1.0f, 0.0f), 90.0f, glm::vec3(24.0f, 2.0f, 0.5f), meshObject, meshIndexCount);
	texture = loadBitmap("outsideBuilding.bmp");
	worldObjects[14] = new object("out1Building1", "Building", texture, glm::vec3(-8.0f, 1.0f, -34.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(8.0f, 15.0f, 8.0f), meshObject, meshIndexCount);
	worldObjects[15] = new object("out1Building2", "Building", texture, glm::vec3(-16.0f, 1.0f, -34.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(8.0f, 15.0f, 8.0f), meshObject, meshIndexCount);
	worldObjects[16] = new object("out1Building3", "Building", texture, glm::vec3(-24.0f, 1.0f, -26.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, glm::vec3(8.0f, 15.0f, 8.0f), meshObject, meshIndexCount);
	texture = loadBitmap("outsideBuilding2.bmp");
	worldObjects[17] = new object("out2Building1", "Building", texture, glm::vec3(8.0f, 1.0f, -34.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(8.0f, 15.0f, 8.0f), meshObject, meshIndexCount);
	worldObjects[18] = new object("out2Building2", "Building", texture, glm::vec3(8.0f, 1.0f, 6.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(8.0f, 15.0f, 8.0f), meshObject, meshIndexCount);
	texture = loadBitmap("oldBuilding.bmp");
	worldObjects[19] = new object("oldBuilding1", "Building", texture, glm::vec3(20.0f, 3.5f, -10.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(8.0f, 10.0f, 8.0f), meshObject, meshIndexCount);
	worldObjects[20] = new object("oldBuilding2", "Building", texture, glm::vec3(20.0f, 3.5f, -18.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(8.0f, 10.0f, 8.0f), meshObject, meshIndexCount);
	worldObjects[21] = new object("oldBuilding3", "Building", texture, glm::vec3(20.0f, 3.5f, -2.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(8.0f, 10.0f, 8.0f), meshObject, meshIndexCount);
	worldObjects[22] = new object("oldBuilding4", "Building", texture, glm::vec3(20.0f, 3.5f, 6.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(8.0f, 10.0f, 8.0f), meshObject, meshIndexCount);
	worldObjects[23] = new object("oldBuilding5", "Building", texture, glm::vec3(20.0f, 3.5f, -26.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(8.0f, 10.0f, 8.0f), meshObject, meshIndexCount);
	texture = loadBitmap("generalStore.bmp");
	worldObjects[24] = new object("Store", "Building", texture, glm::vec3(6.0f, 2.7f, -24.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(4.0f, 6.0f, 10.0f), meshObject, meshIndexCount);
	texture = loadBitmap("derelictBuilding.bmp");
	worldObjects[25] = new object("derelict", "Building", texture, glm::vec3(6.0f, 2.7f, -10.0f), glm::vec3(0.0f, 0.0f, 1.0f), 180.0f, glm::vec3(4.0f, 6.0f, 10.0f), meshObject, meshIndexCount);
	texture = loadBitmap("upacBoxOffice.bmp");
	worldObjects[26] = new object("upac", "Building", texture, glm::vec3(-9.5f, 2.7f, -16.0f), glm::vec3(0.0f, 1.0f, 0.0f), 180.0f, glm::vec3(11.0f, 6.0f, 8.0f), meshObject, meshIndexCount);
}

/*
* Load bitmap file from name taken in.
* @param - char * - name of bitmap file.
* @return - GLuint - returns texture created from bitmap image.
*/
GLuint immovableObjects::loadBitmap(char *fname)
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	// load file - using core SDL library
 	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 

	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID;	// return value of texture ID
}

/*
* Draws all immovable objects.
* Creates AABBs in first call for collisions.
* @param - std::stack<glm::mat4> - the current modelview matrix.
* @param - GLuint - current shader to be used.
*/
void immovableObjects::draw(std::stack<glm::mat4> mvStack, GLuint shader)	
{
	rt3d::materialStruct materialEnvironment = {
		{0.4f, 0.2f, 0.2f, 1.0f}, // ambient
		{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse 
		{1.0f, 0.8f, 0.8f, 1.0f}, // specular
		2.0f  // shininess
	};

	rt3d::setMaterial(shader, materialEnvironment);

	for(int i = 0; i < MAX_OBJECTS; i++)
	{
		glUseProgram(shader);
		mvStack.push(mvStack.top());
		glBindTexture(GL_TEXTURE_2D, worldObjects[i]->getTex());
		mvStack.top() = glm::translate(mvStack.top(), worldObjects[i]->getTranslate());		
		mvStack.top() = glm::rotate(mvStack.top(), worldObjects[i]->getRotation(), worldObjects[i]->getRotate());		
		mvStack.top() = glm::scale(mvStack.top(), worldObjects[i]->getScale());		
		rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));	
		rt3d::drawIndexedMesh(worldObjects[i]->getMeshObject(),worldObjects[i]->getMeshIndexCount(),GL_TRIANGLES);
		
		//Sets AABB for world objects, only set once as bool will be true after first call.
		if(AABBset == false)
		{				
			std::vector<GLfloat> tmpVerts = verts;
			for(int i = 0; i < tmpVerts.size(); i=i+3)
			{
				glm::vec4 tempVec = mvStack.top() * glm::vec4(tmpVerts[i], tmpVerts[i+1], tmpVerts[i+2], 1.0);
				tmpVerts[i] = tempVec.x;
				tmpVerts[i+1] = tempVec.y;
				tmpVerts[i+2] = tempVec.z;	
			}
			boxes[i] = new AABB(worldObjects[i]->getName());
			boxes[i]->setAABB(tmpVerts, tmpVerts.size());	
			//Some tweaking required as not rotated at origin.
			boxes[i]->updateAABB(glm::vec3(boxes[i]->getMax().x - boxes[i]->getLength()/2 + 0.9, boxes[i]->getMax().y - boxes[i]->getHeight()/2 + 1.0, boxes[i]->getMax().z - boxes[i]->getDepth()/2));
		}		
		mvStack.pop();
	}	
	if(AABBset == false)
		AABBset = true;
}
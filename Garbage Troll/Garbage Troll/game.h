//(c) Michael O'Neil 2015

#include "rt3d.h"
#include <SDL_ttf.h>
#include <stack>
#include <vector>
#include <ctime>
#include "gameState.h"

#ifndef GAME_H  
#define GAME_H

/*
* Main Game Class, will initialise everything required for all states,
* will draw and update functions for the current state.
*/
class Game
{
private:	
	TTF_Font * textFont;	
	SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
	
	GLuint shaderProgram; //shader programs	
	GLuint depthOffProgram;
	GLuint meshObject; //used for objects
	
	gameState * currentState; //States
	gameState * State_Intro;
	gameState * State_MainMenu;
	gameState * State_Credits;
	gameState * State_Play;
	gameState * State_Table;

	int playerScore; //game variables
	std::string playerName;
	bool running;
	bool inGame;
public:	
	Game(void); 	
	~Game(void);
	void init(void);
	SDL_Window * setupRC(SDL_GLContext &context);
	GLuint textToTexture(const char * str, glm::vec3 col);	

	gameState *getState(void) {return currentState;}
	gameState *getIntroState(void) {return State_Intro;}
	gameState * getMainMenuState(void) {return State_MainMenu;}
	gameState * getCreditsState(void) {return State_Credits;}
	gameState * getPlayState(void) {return State_Play;}
	gameState * getTableState(void) {return State_Table;}
	
	void setState(gameState* newState);			
	int getScore(void) {return playerScore;}
	std::string getName(void) {return playerName;}
	bool getInGame(void) {return inGame;}
	void setScore(int s) {playerScore = s;}
	void setInGame(bool set) {inGame = set;}
	void setName(std::string name) {playerName = name;}	
	void setRunning(bool set) {running = set;}
	void run(void);		
};

#endif